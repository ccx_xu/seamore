<?php 
	require_once dirname(__FILE__) . '/db/DB.class.php';
	$id = $_REQUEST['id'];

	$db = new DB();

	$sql = "select id, name, name_cn, name_en, (select name from mz_category where id_key = mw.cate_id limit 0,1) as cate_name, image_url, order_num, c_name, s_date, s_name, descc, state from mz_work mw where id = ? ";
	$stmt = $db -> prepare($sql);
	$stmt->bind_param("s", $p1);
	 
	// 设置参数并执行
	$p1 = $id;
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($r1, $r2, $r3, $r4, $r5, $r6, $r7, $r8, $r9, $r10, $r11, $r12);
	if ($stmt->fetch())
    {
    	//  当验证通过后，启动 Session
    	// session_start();
    	//  注册登陆成功的 admin 变量，并赋值 true
    	$wname = $r2;
    	$name_cn = $r3;
    	$name_en = $r4;
    	$cate_name = $r5;
    	$image_url = $r6;
    	$order_num = $r7;
    	$c_name = $r8;
    	$s_date = $r9;
    	$s_name = $r10;
    	$descc = $r11;
    	$state = $r12;
    } 

    $sql0 = "select file_path from mz_work_detail ".
    		"where state='1' and main_flg='1' and work_id=? order by order_num limit 0,1";
	$stmt0 = $db -> prepare($sql0);
	$stmt0->bind_param("s", $p01);
	// 设置参数并执行
	$p01 = $id;
	// 处理打算执行的SQL命令
	$stmt0->execute();
	// 执行SQL语句
	$stmt0->store_result();
	// 输出查询的记录个数
    $stmt0->bind_result($r01);
	if ($stmt0->fetch())
    {
    	$imageSrc0 = $r01;
    } 

    $sql2 = "select file_path from mz_work_detail ".
    		"where state='1' and main_flg='0' and work_id=? order by order_num";
	$stmt2 = $db -> prepare($sql2);
	$stmt2->bind_param("s", $p02);
	// 设置参数并执行
	$p02 = $id;
	// 处理打算执行的SQL命令
	$stmt2->execute();
	// 执行SQL语句
	$stmt2->store_result();
	// 输出查询的记录个数
    $stmt2->bind_result($r02);
    $imageArr = array();
	while ($stmt2->fetch())
    {
    	$imageArr[] = $r02;
    }

    $works = array();
    $dbNav = new DB();
    $sql = "select id, name, name_cn, name_en, image_url from mz_work where state='1' ORDER BY RAND() LIMIT 5";
    $stmtNav = $dbNav -> prepare($sql);
    // 处理打算执行的SQL命令
    $stmtNav->execute();
    // 执行SQL语句
    $stmtNav->store_result();
    // 输出查询的记录个数
    $stmtNav->bind_result($id, $name, $nameCn, $nameEn, $imageUrl);
    while ($stmtNav->fetch())
    {
        $tmpArr = array();
        $tmpArr['id'] = $id;
        $tmpArr['name'] = $name;
        $tmpArr['nameCn'] = $nameCn;
        $tmpArr['nameEn'] = $nameEn;
        $tmpArr['imageUrl'] = $imageUrl;
        $works[] = $tmpArr;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="" class="csstransforms csstransforms3d csstransitions">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<title><?php echo $name_cn ?> - 西美品牌策划</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta name="renderer" content="webkit"> 
		<meta name="keywords" content="形象策划,橙子,便利店">
		<meta name="description" content="形象策划,橙子,便利店-宇宙第一品牌">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">
		
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<link rel="stylesheet" type="text/css" href="/css/main.css">
		<link rel="stylesheet" type="text/css" href="/css/style_page.css">

		<script type="text/javascript" language="javascript" src="/js/css3-mediaqueries.js"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/jquery.SuperSlide.2.1.1.js"></script>

		<!--[if lte IE 6]>
		<script src="http://www.hibona.cn/statics/js/png.js" type="text/javascript"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('div, ul, img, li, input , a');
			</script>
		<![endif]--> 

		<script type="text/javascript">
			$(function(){
				$('.workList,.footMenu').isotope({
				  // options
				  itemSelector : 'li',
				  layoutMode : 'fitRows'
				});
				$('.workList li').hover(function(){
					
					$(this).find(".mask").css({opacity:0});
					$(this).find(".mask").show();
					$(this).find(".mask").animate({opacity:1}); 
				},function(){
					
					$(this).find(".mask").animate({opacity:0}); 
				});
				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});
			});

			$(function(){
				function dropNav(){
					var $btn=$(".nav_img"),
						$Mn=$(".nav_u_down2"),
						$true=true;
						$Mn.fadeOut();	
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
			});
			
			$(function(){
				$(".select").each(function(){
					var s=$(this);
					var z=parseInt(s.css("z-index"));
					var dt=$(this).children("dt");
					var dd=$(this).children("dd");
					var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",999);};   //展开效果
					var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",999);};    //关闭效果
					dt.click(function(){dd.is(":hidden")?_show():_hide();});
					dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
					$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
				})
			});
		</script>
		<script type="text/javascript" src="/js/mlb.js"></script>
	</head>
	<body>
		<?php include 'header.php';?>
		<script id="jsID" type="text/javascript">
			jQuery("#nav").slide({ 
				type:"menu", 
				titCell:".nLi", 
				targetCell:".sub",
				effect:"slideDown",
				delayTime:300,
				triggerTime:0,
				defaultPlay:false,
				returnDefault:true
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".side ul li").hover(function(){
					$(this).find(".sidebox").stop().animate({"width":"124px"},200).css({"opacity":"1","filter":"Alpha(opacity=100)","background":"#ae1c1c"})	
					$(this).find(".sidebox2").stop().animate({"width":"174px"},200).css({"opacity":"1","filter":"Alpha(opacity=100)","background":"#ae1c1c"})	
				},function(){
					$(this).find(".sidebox").stop().animate({"width":"50px"},200).css({"opacity":"0.8","filter":"Alpha(opacity=80)","background":"#000"})	
					$(this).find(".sidebox2").stop().animate({"width":"50px"},200).css({"opacity":"0.8","filter":"Alpha(opacity=80)","background":"#000"})	
				});
			});
			//回到顶部
			function goTop(){
				$('html,body').animate({'scrollTop':0},600); //滚回顶部的时间，越小滚的速度越快~
			}
		</script>
		<style>
			.side{position:fixed;width:50px;height:275px;right:0;top:410px;z-index:100;}
			.side ul li{width:50px;height:51px;float:left;position:relative; }
			.side ul li .sidebox{position:absolute;width:50px;height:50px;top:0;right:0;transition:all 0.3s;background:#000;opacity:0.8;filter:Alpha(opacity=80);color:#fff;font:14px/50px "微软雅黑";overflow:hidden; border-radius:6px 0 0 6px; z-index:99;}
			.side ul li .sidetop{width:50px;height:50px;line-height:50px;display:inline-block;background:#000;opacity:0.8;filter:Alpha(opacity=80);transition:all 0.3s; border-radius:6px 0 0 6px;}
			.side ul li .sidetop:hover{background:#ae1c1c;opacity:1;filter:Alpha(opacity=100);}
			.side ul li img{float:left;}
			.side ul li .sidebox2{position:absolute;width:50px;height:50px;top:0;right:0;transition:all 0.3s;background:#000;opacity:0.8;filter:Alpha(opacity=80);color:#fff;font:14px/50px "微软雅黑";overflow:hidden; border-radius:6px 0 0 6px;}
			.side ul li .sidetop3{width:50px;height:50px;line-height:50px;display:inline-block;background:#000;opacity:0.8;filter:Alpha(opacity=80);transition:all 0.3s; border-radius:6px 0 0 6px; cursor:pointer; z-index:2;}
			.side ul li .sidetop3:hover{background:#ae1c1c;opacity:1;filter:Alpha(opacity=100);}
			.side ul li .sidetop3:hover .gr{opacity:1; transition:all 0.5s; left:-150px;}
			.gr{ opacity:0; position:absolute; left:-150px; bottom:0; transition:all 0.5s; z-index:0;}
		</style>
		<div class="side">
			<ul>
				<li><div class="sidebox2"><img src="./images/side_icon01.png" width="50">187-6581-7278</div></li>
				<li>
					<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=444166815&amp;site=qq&amp;menu=yes" target="_blank">
						<div class="sidebox"><img src="./images/side_icon04.png" width="50">QQ客服</div>
					</a>
				</li>
				<li>
					<div class="sidetop3">
						<img src="./images/side_icon03.png" width="50">
						<div class="gr"><img src="./images/weixin_contact.jpg" width="150"></div>
					</div>
				</li>
				<li style="border:none;"><a href="javascript:goTop();" class="sidetop"><img src="./images/side_icon05.png" width="50"></a></li>
			</ul>
		</div>
		<div class="banner_page">
			<div class="banner_page2" style="background-image:url(<?php echo $imageSrc0 ?>);"></div>
		</div>
		<div class="prod_cont" style="background:">
			<div class="prod_top">
				<div class="prod_zhong">
					<h2 class="name_en"><?php echo $wname ?></h2>
					<p class="name_cn"><?php echo $name_cn ?></p>
					<div class="prod_text">
						<div class="text_left">
							<?php include ".".$descc ?>
						</div>
						<div class="text_right">
							客户名称：<?php echo $c_name ?> <br> 
							所属行业：<?php echo $cate_name ?><br> 
							服务时间：<?php echo $s_date ?><br> 
							服务项目：<?php echo $s_name ?><br> 
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
	
			<div class="prod_img" style="background:; ">
				<div class="prod_zhong">
					<?php  for ($i=0; $i<count($imageArr); $i++) {	?>
					<img src="<?php echo $imageArr[$i] ?>">
					<?php } ?>
					<img src="/images/tu_bottom.jpg" alt="">
					<div class="blank6"></div>
					<div class="blank6"></div>
					<div class="blank6"></div>
					<div class="blank6"></div>
					<div class="blank6"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="prod_foot1">
			<ul class="workList isotope" style="position: relative; overflow: hidden; height: 382px;">
                <?php for($i=0; $i<count($works);$i++) { ?>
				<li class="isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 60px, 0px);">
					<a href="/detail.php?id=<?php echo $works[$i]['id'] ?>">
						<img src="<?php echo $works[$i]['imageUrl'] ?>" width="368" height="243" alt="">
						<div class="prd_name"> 
							<strong class="e_name"><?php echo $works[$i]['name'] ?></strong>
							<p class="cn_name"><?php echo $works[$i]['nameCn'] ?></p>
							<p class="en_name"><?php echo $works[$i]['nameEn'] ?></p>
						</div>
					</a> 
				</li>
				<?php } ?>
				<div class="clear"></div>
			</ul>
			<a href="/works.php" class="foot_gd"></a>	
			<a href="#" class="fh_s"></a>		
		</div>
		<?php include './common/foot01.html';?>
	</body>
</html>