<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0064)http://www.hibona.cn/index.php?m=content&c=index&a=lists&catid=9 -->
<html xmlns="http://www.w3.org/1999/xhtml" class="csstransforms csstransforms3d csstransitions" style="">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="”renderer”" content="”webkit”"> 
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<title>西美品牌策划</title>
		<meta name="keywords" content="西美品牌策划">
		<meta name="description" content="西美品牌策划，宇宙第一品牌">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">

		
		<link rel="stylesheet" type="text/css" href="./css/style.css?v=3">
		<link rel="stylesheet" type="text/css" href="./css/main.css">
		<style type="text/css">
			.ab_t1_img img {
			    padding-bottom: 50px;
			}
		</style>

		<script type="text/javascript" language="javascript" src="/js/css3-mediaqueries.js"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/jquery.SuperSlide.2.1.1.js"></script>
		
		<!--[if lte IE 6]>
		<script src="/js/png.js" type="text/javascript"></script>
		    <script type="text/javascript">
		        DD_belatedPNG.fix('div, ul, img, li, input , a');
		    </script>
		<![endif]--> 
		<script type="text/javascript">
			$(function(){

				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
				
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});
				
				
			});

			$(function(){
				 function dropNav(){
				var $btn=$(".nav_img"),
					$Mn=$(".nav_u_down2"),
					$true=true;
					$Mn.fadeOut();	
					
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
		 
			})
	

		</script>
		<script type="text/javascript" src="/js/mlb.js"></script>
	</head>
	<body>
		<?php include 'header.php';?>
		<script id="jsID" type="text/javascript">
			jQuery("#nav").slide({ type:"menu", titCell:".nLi", targetCell:".sub",effect:"slideDown",delayTime:300,triggerTime:0,defaultPlay:false,returnDefault:true});
		</script>
		<div class="banner_about" style=" background-image:url(./images/about_banner.jpg);"></div>
		<div class="about_cont">
			<div class="about_t1">
				<div class="ab_title">
					<h2>欢迎来到国内领先的品牌规划与形象设计公司</h2>
					<p>帮助远见卓识的商业领导者探索核心问题，开创价值新领地</p>
				</div>
				<div class="ab_text_box">
					<div class="ab_text_left">
						<h2>携手海右博纳，从优秀到卓越</h2>
						济南海右博纳广告有限公司是国内领先的品牌规划与设计公司，致力于通过科学有效的、国际化的品牌策略、设计与管理，帮助拥有品牌梦想的企业塑造独特而持久的国际化品牌形象，实现品牌的可持续盈利。目前，作为业内极具创新精神的品牌创意公司，团队集结了品牌、数字、空间、产品等资深的设计师与品牌策略顾问。与此同时，海右博纳还与国内外知名专家学者及众多知名设计学院的行业精英保持着紧密的交流与合作。海右博纳结合全球化的品牌经验，运用国际化的标准方法，为客户提供品牌策划、品牌设计、数字互动、空间设计、品牌管理等服务，现已为国内外数百家企业提供了全方位的品牌整合解决方案。
					</div>
					<div class="ab_text_right">
						<h2>热爱，不知疲倦</h2>
						品牌源于梦想，海右博纳正是一群追梦人。从创立的第一天起（2005年创立），就立志以成为成为最具远见的公司、业内的佼佼者以及企业取得成功不可或缺的伙伴。信念驱动我们的战略，战略让我们则无旁骛。这种雄心壮志源于我们的实力，并指引我们不断向前。海右博纳是合伙人共同所有的独立咨询机构，我们也在不断招募顶级人才加入我们，打造一家伟大的公司是我们的共同理想，因为热爱所以不知疲倦，我们致力于无所畏惧的探索埋藏于黑暗中的潜在未来，捕捉一闪而过的创意瞬间，勇敢尝试新的想法，我们追求在其他人还未提出问题之前就对它有了完美解答。
					</div>
					<div class="clear"></div>
				</div>
				<div class="ab_t1_img">
					<img src="./images/ab_img1.jpg" alt="">
					<img src="./images/ab_img2.png" alt="">
				</div>
			</div>
		</div>
		<?php include './common/foot01.html';?>
	</body>
</html>