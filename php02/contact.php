<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="" class="csstransforms csstransforms3d csstransitions">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<title>西美品牌策划</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta name="renderer" content="webkit"> 
		<meta name="keywords" content="西美品牌策划">
		<meta name="description" content="西美品牌策划">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">
		
		<link rel="stylesheet" type="text/css" href="./css/main.css">
		<link rel="stylesheet" type="text/css" href="./css/style.css?v=1">
		<style type="text/css">
			.foot_bbt {
			    background: #1F2134;
			}
		</style>

		<script type="text/javascript" language="javascript" src="./js/css3-mediaqueries.js"></script>
		<script type="text/javascript" language="javascript" src="./js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="./js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="./js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="./js/jquery.SuperSlide.2.1.1.js"></script>

		<!--[if lte IE 6]>
		<script src="http://www.hibona.cn/statics/js/png.js" type="text/javascript"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('div, ul, img, li, input , a');
			</script>
		<![endif]--> 
		<script type="text/javascript">
			$(function(){
				$('.workList,.footMenu').isotope({
				  // options
				  itemSelector : 'li',
				  layoutMode : 'fitRows'
				});
				$('.workList li').hover(function(){
					$(this).find(".mask").css({opacity:0});
					$(this).find(".mask").show();
					$(this).find(".mask").animate({opacity:1}); 
				},function(){
					$(this).find(".mask").animate({opacity:0}); 
				});
				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});				
			});

			$(function(){
				 function dropNav(){
				var $btn=$(".nav_img"),
					$Mn=$(".nav_u_down2"),
					$true=true;
					$Mn.fadeOut();	
					
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
		 
			});
	
			
		</script>
		<script type="text/javascript" src="/js/mlb.js"></script>
	</head>
	<body>
		<?php include 'header.php';?>
		<script id="jsID" type="text/javascript">
			jQuery("#nav").slide({ type:"menu", titCell:".nLi", targetCell:".sub",effect:"slideDown",delayTime:300,triggerTime:0,defaultPlay:false,returnDefault:true});
		</script>
		<div class="banner_about" style=" background-image:url(./images/lx_banner.jpg);"></div>
		<div class="lx_cont_box">
			<div class="lx_topz">
				<div class="lxt_left">
					<div class="wenzi1">即刻合作，让世界为我们惊叹。</div>
					<div class="lx_img"><img src="./images/lx_img.png" alt="西美品牌地图"></div>
				</div>
				<div class="lxt_right">
					<div class="lx_ten">CONTACT US</div>
					<div class="lx_tt2">
						<p>SEEMORE Brand Planning</p>
						<span>西美品牌策划</span>
					</div>
					<div class="lx_tt3" style="height:120px;">地址：济南市天桥区滨河商务中心C座8层<br>
						品牌专线：187-6581-7278<br>
					</div>
					<div class="lx_erm"><img src="./images/weixin_contact.jpg" alt="西美品牌"></div>
				</div>
				<div class="clear"></div>
			</div>
			
			<?php include './common/foot01.html';?>
		</div>
	</body>
</html>