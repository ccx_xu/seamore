<?php 
	require_once dirname(__FILE__) . '/db/DB.class.php';
	$nav = array();
	$dbNav = new DB();
	$sql = "select id, name, name_en, static_url from mz_navigation where state='1' order by order_num ";
	$stmtNav = $dbNav -> prepare($sql);
	// 处理打算执行的SQL命令
	$stmtNav->execute();
	// 执行SQL语句
	$stmtNav->store_result();
	// 输出查询的记录个数
    $stmtNav->bind_result($id, $name, $nameEn, $staticUrl);
	while ($stmtNav->fetch())
    {
    	$tmpArr = array();
    	$tmpArr['id'] = $id;
    	$tmpArr['name'] = $name;
    	$tmpArr['nameEn'] = $nameEn;
    	$tmpArr['staticUrl'] = $staticUrl;
    	$nav[] = $tmpArr;
    } 
?>
<div class="nav_box">
	<div class="jvzhongdd" style="width: 95%; margin: auto;">
		<div class="logo"><a href="."><img src="/images/logo.png" alt=""></a></div>
		<div class="right_nav">
			<ul id="nav" class="nav clearfix">		
				<?php for($i=0; $i<count($nav); $i++) { ?>			
				<li class="nLi">
					<h3>
						<a href="<?php echo $nav[$i]['staticUrl'] ?>">
							<span class="n_e"><?php echo $nav[$i]['nameEn'] ?></span>
							<span class="n_c"><?php echo $nav[$i]['name'] ?></span>
						</a>
					</h3>
				</li>				
				<?php } ?>
			</ul>				
		</div>
	</div>
</div>