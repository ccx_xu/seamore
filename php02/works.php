<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0065)http://www.hibona.cn/index.php?m=content&c=index&a=lists&catid=14 -->
<html xmlns="http://www.w3.org/1999/xhtml" style="" class="csstransforms csstransforms3d csstransitions">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>西美品牌策划</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<meta name="renderer" content="webkit"> 
		<meta name="keywords" content="西美品牌策划">
		<meta name="description" content="西美品牌策划">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">
		<link rel="stylesheet" type="text/css" href="/css/style.css?v=1">
		<link rel="stylesheet" type="text/css" href="/css/main.css">
		
		<script type="text/javascript" language="javascript" src="/js/css3-mediaqueries.js"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
		<script type="text/javascript" src="/js/jquery.superslide.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/index.js"></script>

		<!--[if lte IE 6]>
		<script src="http://www.hibona.cn/statics/js/png.js" type="text/javascript"></script>
		    <script type="text/javascript">
		        DD_belatedPNG.fix('div, ul, img, li, input , a');
		    </script>
		<![endif]--> 

		<script type="text/javascript">
			$(function(){
				$('.workList,.footMenu').isotope({
				  // options
				  itemSelector : 'li',
				  layoutMode : 'fitRows'
				});
				$('.workList li').hover(function(){
					
					$(this).find(".mask").css({opacity:0});
					$(this).find(".mask").show();
					$(this).find(".mask").animate({opacity:1}); 
				},function(){
					
					$(this).find(".mask").animate({opacity:0}); 
				});
				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer,.foot_bb").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
				
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});
				
				
			});

			$(function(){
				 function dropNav(){
				var $btn=$(".nav_img"),
					$Mn=$(".nav_u_down2"),
					$true=true;
					$Mn.fadeOut();	
					
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
		 
			})
	
			/*下拉导航*/
			$(function(){
				/*============================
				@author:flc
				@time:2014-02-11 18:16:09
				@qq:3407725
				============================*/
				$(".select").each(function(){
					var s=$(this);
					var z=parseInt(s.css("z-index"));
					var dt=$(this).children("dt");
					var dd=$(this).children("dd");
					var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",999);};   //展开效果
					var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",999);};    //关闭效果
					dt.click(function(){dd.is(":hidden")?_show():_hide();});
					dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
					$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
				})
			})

		</script>
		<script>
		    
		</script>
	</head>
	<body>
		<?php include 'header.php';?>
		<div class="warp2" style="margin-top:90px;margin-bottom:20px;">
			<div class="warp-type">
					<p><a href="/works.php" class="museo-light">SEE   OUR   WORK</a></p>
					<div class="warp-type-list">
							<a href="/works.php" target="_blank" class="active museo-light">All</a>
							<a href="/works.php" target="_blank" rel="nofollow">品牌策划</a>
							<a href="/works.php" target="_blank" rel="nofollow">品牌设计</a>
							<a href="/works.php" target="_blank" rel="nofollow">LOGO VI设计</a>
							<a href="/works.php" target="_blank" rel="nofollow">包装策划设计</a>
					</div>
			</div>
			<?php 
				$works = array();
				$dbNav = new DB();
				$sql = "select id, name, name_cn, name_en, image_url from mz_work where state='1' order by order_num ";
				$stmtNav = $dbNav -> prepare($sql);
				// 处理打算执行的SQL命令
				$stmtNav->execute();
				// 执行SQL语句
				$stmtNav->store_result();
				// 输出查询的记录个数
				$stmtNav->bind_result($id, $name, $nameCn, $nameEn, $imageUrl);
				while ($stmtNav->fetch())
				{
					$tmpArr = array();
					$tmpArr['id'] = $id;
					$tmpArr['name'] = $name;
					$tmpArr['nameCn'] = $nameCn;
					$tmpArr['nameEn'] = $nameEn;
					$tmpArr['imageUrl'] = $imageUrl;
					$works[] = $tmpArr;
				} 
			?>
			<ul class="case clear">
				<?php for($i=0; $i<count($works);$i++) { ?>
					<a href="/detail.php?id=<?php echo $works[$i]['id'] ?>" target="_blank">
					<li>
						<img src="<?php echo $works[$i]['imageUrl'] ?>" class="lazy tran imgbig" alt="<?php echo $works[$i]['nameCn'] ?>" data-original="<?php echo $works[$i]['imageUrl'] ?>" style="display: none; transform: scale(1);">
						<div class="case-summary" style="display: none; opacity: 1;">
							<div style="position: relative;width:100%;height: 100%">
								<p class="p1"><?php echo $works[$i]['name'] ?></p>
								<p class="p2">-<br><span class="syl"><?php echo $works[$i]['nameCn'] ?></span>
													<br> <?php echo $works[$i]['nameEn'] ?>                            
								</p>
							</div>
						</div>
				</li>
					</a>
				<?php } ?>		
			</ul>
		</div>
		<?php include './common/foot01.html';?>
	</body>
</html>