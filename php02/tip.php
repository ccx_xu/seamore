<?php 
	include dirname(__FILE__) . '/db/DB.class.php';

	$result = "";

	$arr = array();
	$db = new DB();
	$name = $_POST['name'];
	$gsname = $_POST['gsname'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$cont = $_POST['cont'];
	$sql = "insert into mz_message(id, name, gsname, phone, email, cont, sub_time) values(?,?,?,?,?,?,?)";
	$stmt = $db -> prepare($sql);
	$stmt->bind_param("sssssss", $p1, $p2, $p3, $p4, $p5, $p6, $p7);
 
	// 设置参数并执行
	$p1 = $db->uuid();
	$p2 = $name;
	$p3 = $gsname;
	$p4 = $phone;
	$p5 = $email;
	$p6 = $cont;
	$p7 = date("Y-m-d H:i",time());
	// 处理打算执行的SQL命令
	$stmt->execute();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=7">
		<title>提示信息</title>
		<style type="text/css">
			*{ 
				padding:0; 
				margin:0; 
				font-size:12px
			}
			.showMsg .guery {
				white-space: pre-wrap; /* css-3 */
				white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
				white-space: -pre-wrap; /* Opera 4-6 */
				white-space: -o-pre-wrap; /* Opera 7 */	
				word-wrap: break-word; /* Internet Explorer 5.5+ */
			}
			a:link,a:visited {
				text-decoration:none;
				color:#0068a6;
			}
			a:hover,a:active { 
				color:#ff6600;
				text-decoration: underline;
			}
			.showMsg { 
				border: 1px solid #1e64c8; 
				zoom:1; 
				width:450px; 
				height:174px;
				position:absolute;
				top:50%;
				left:50%;
				margin:-87px 0 0 -225px
			}
			.showMsg h5 {
				background-image: url(http://www.hibona.cn/statics/images//msg_img/msg.png);
				background-repeat: no-repeat; 
				color:#fff; 
				padding-left:35px; 
				height:25px; 
				line-height:26px;
				*line-height:28px; 
				overflow:hidden; 
				font-size:14px; 
				text-align:left
			}
			.showMsg .content { 
				padding:46px 12px 10px 45px; 
				font-size:14px; 
				height:66px;
			}
			.showMsg .bottom { 
				background:#e4ecf7; 
				margin: 0 1px 1px 1px;
				line-height:26px; 
				*line-height:30px; 
				height:26px; 
				text-align:center
			}
			.showMsg .ok,.showMsg .guery { 
				background: url(http://www.hibona.cn/statics/images//msg_img/msg_bg.png) no-repeat 0px -560px;
			}
			.showMsg .guery { 
				background-position: left -460px;
			}
		</style>
		<script type="text/javaScript" src="/js/jquery-1.8.0.min.js"></script>
		<script language="JavaScript" src="/js/admin_common.js"></script>
	</head>
	<body>
		<div class="showMsg" style="text-align:center">
			<h5>提示信息</h5>
    		<div class="content guery" style="display:inline-block;display:-moz-inline-stack;zoom:1;*display:inline; max-width:280px">您的邮箱 不符合要求</div>
    		<div class="bottom">
    			<a href="javascript:history.back();">[点这里返回上一页]</a>
		    </div>
		</div>
		<script style="text/javascript">
			function close_dialog() {
				window.top.location.reload();window.top.art.dialog({id:""}).close();
			}
		</script>
	</body>
</html>