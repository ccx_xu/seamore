<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="" class="csstransforms csstransforms3d csstransitions">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<title>西美品牌策划</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta name="renderer" content="webkit"> 
		<meta name="keywords" content="西美品牌策划">
		<meta name="description" content="西美品牌策划">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">
		
		<link rel="stylesheet" type="text/css" href="./css/style.css?v=555">
		<link rel="stylesheet" type="text/css" href="./css/main.css">

		<script type="text/javascript" language="javascript" src="./js/css3-mediaqueries.js"></script>
		<script type="text/javascript" language="javascript" src="./js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="./js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="./js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="./js/jquery.SuperSlide.2.1.1.js"></script>

		<!--[if lte IE 6]>
		<script src="/js/png.js" type="text/javascript"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('div, ul, img, li, input , a');
			</script>
		<![endif]--> 
		<script type="text/javascript">
			$(function(){
				$('.workList,.footMenu').isotope({
				  // options
				  itemSelector : 'li',
				  layoutMode : 'fitRows'
				});
				$('.workList li').hover(function(){
					
					$(this).find(".mask").css({opacity:0});
					$(this).find(".mask").show();
					$(this).find(".mask").animate({opacity:1}); 
				},function(){
					
					$(this).find(".mask").animate({opacity:0}); 
				});
				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
				
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});
			});

			$(function(){
				 function dropNav(){
				var $btn=$(".nav_img"),
					$Mn=$(".nav_u_down2"),
					$true=true;
					$Mn.fadeOut();	
					
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
		 
			})

		</script>
		<script type="text/javascript" src="/js/mlb.js"></script>
	</head>
	<body>
		<?php include 'header.php';?>
		<div class="news">
            <div class="news-type">
                <h3 class="museo-light ">NEWS</h3>
                <ul class="clear">
                    <li><a href="/viewpoint.php">品牌策划观点</a></li>
                </ul>
            </div>
            <div class="news-list" style="border:none">
                <ul>
                    <li class="clear">
                        <a href="/news/1038.php" class="imgbigout" target="_blank">
                            <img data-original="/image/2020/04/1587173657_1104.jpg" class="lazy imgbig" src="./hhhh/1587173657_1104.jpg" style="display: none;">
                        </a>
                        <div class="news-summary">
                            <h4>2020-09-08</h4>
                            <p><a href="/news/1038.php" class="syl" target="_blank">牛奶包装设计需要突出的重点有哪些？</a></p>
                            <span><a href="/news/1038.php" target="_blank">不管从事的是什么行业，不管你在生活中遇到什么问题，很多时候都是会有它的技巧的，只要抓住了这些技巧...</a></span>
                            <a href="/news/1038.php" class="more" target="_blank">Read the article</a>
                        </div>
                    </li>
                    <li class="clear">
                        <a href="/news/1038.php" class="imgbigout" target="_blank">
                            <img data-original="/image/2020/04/1587173500_8078.jpg" class="lazy imgbig" src="./hhhh/1587173500_8078.jpg" style="display: none;">
                        </a>
                        <div class="news-summary">
                            <h4>2020-09-08</h4>
                            <p><a href="/news/1038.php" class="syl" target="_blank">食品包装设计要具备哪些功能？这些不容忽视！</a></p>
                            <span><a href="/news/1038.php" target="_blank">食品包装设计说到底就是为了吸引消费者的注意力，并且让他们产生购买的欲望。另外就是提升品牌在消费者...</a></span>
                            <a href="/news/1038.php" class="more" target="_blank">Read the article</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="pager">
                <a class="current disabled">&lt;&lt;</a>
                <a class="current disabled">&lt;</a>
                <a class="now">1</a>
                <a title="2" href="/viewpoint.php">2</a>
                <a title="3" href="/viewpoint.php">3</a>
                <a title="4" href="/viewpoint.php">4</a>
                <a title="5" href="/viewpoint.php">5</a>
                <a title="6" href="/viewpoint.php">6</a>
                <a href="/viewpoint.php">...</a>
                <a title="12" href="/viewpoint.php">12</a>
                <a class="p3" title="&gt;" href="/viewpoint.php">&gt;</a>
                <a class="p4" title="&gt;&gt;" href="/viewpoint.php">&gt;&gt;</a>        
            </div>
        </div>
		<?php include './common/foot01.html';?>
	</body>
</html>