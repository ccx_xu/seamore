<?php
	$cates = array();
	$db = new DB();
	$sql = "select id_key, name from mz_category where state='1'";
	$stmt = $db -> prepare($sql);
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($key, $name);
	while ($stmt->fetch())
    {
    	$tmpArr = array();
    	$tmpArr['key'] = $key;
    	$tmpArr['text'] = $name;
    	$cates[] = $tmpArr;
    } 
?>
<div class="hd">
	<div class="hd_title">西美品牌—欢迎来到国内领先的品牌策划与创意机构！<br>
		<span>SEEMORE—DOMESTIC LEADING BRAND PLANNING AND CREATIVE ORGANIZATION！</span>
	</div>
	<div class="lobo" style="width:138px;"><img src="/images/hi_navimg.png" alt=""></div>
	<div class="demo" style="width:140px;">
		
		<dl class="select" style="z-index: 999;">
			<dt>行业类别：ALL</dt>
			<dd style="">
				<ul>
					<?php  for ($i=0; $i<count($cates); $i++) {	?>
					<li><a href="/index.php?catid=<?php echo $cates[$i]['key'] ?>"><?php echo $cates[$i]['text'] ?></a></li>
					<?php } ?>
				</ul>
			</dd>
		</dl>
	</div>
</div>