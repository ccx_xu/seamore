<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="" class="csstransforms csstransforms3d csstransitions">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="/images/mlb_ico.png"/>
		<title>西美品牌策划</title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta name="renderer" content="webkit"> 
		<meta name="keywords" content="西美品牌策划">
		<meta name="description" content="西美品牌策划-宇宙第一品牌">
		<meta name="viewport" content="target-densitydpi=device-dpi,width=420,user-scalable=0">
		<link rel="stylesheet" type="text/css" href="/css/style.css?v=5" />
		<link rel="stylesheet" type="text/css" href="/css/main.css" />
		<script type="text/javascript" language="javascript" src="/js/css3-mediaqueries.js?v=1"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.bxSlider.min.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
		<script type="text/javascript" src="/js/jquery.superslide.js"></script>
		<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>		
		<script type="text/javascript" src="/js/index.js"></script>

		<!--[if lte IE 6]>
		<script src="/js/png.js" type="text/javascript"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('div, ul, img, li, input , a');
			</script>
		<![endif]--> 

		<script type="text/javascript">
			$(function(){
				$(window).bind("resize", resize);
				function resize(){
					var $headWidth = $(window).width()>1000?$(window).width():1000;
					var $itemWidth=368;
					var $wrapperWidth=$itemWidth*parseInt(($(window).width()-10)/$itemWidth);
					$(".jvzhongdd").css({width:$headWidth});
					$(".jvzhongdd,.wrapper,.ind_ff,.footer,.foot_bb").css({width:$wrapperWidth,margin:"auto"});

				}
				resize();
	
				var slider1=$("#bigImg ul").bxSlider({controls:false,auto:true,pause:6000,mode:'fade',randomStart:true});
	
			});

			$(function(){
				function dropNav(){
					var $btn=$(".nav_img"),
						$Mn=$(".nav_u_down2"),
						$true=true;
						$Mn.fadeOut();	
			
					$btn.bind("click",function (e){
						if($true){
							$Mn.fadeIn();
							$true=false;
						}else{
							$Mn.fadeOut();
							$true=true;
						}
						e.preventDefault();
						return false;
					});
					$Mn.bind("click" ,function (e){
						$Mn.fadeIn();
						$true=false;
						e.stopPropagation();
					});
					$(document).bind("click" ,function (){
						$Mn.fadeOut();
						$true=true;
					});
				};
				$(function (){
					dropNav();	
				});
			});
		</script>
		<script>
			function rf() {
				return false; 
			}
			document.oncontextmenu = rf;
			function keydown() {
				if(event.ctrlKey ==true || event.keyCode ==93 || event.shiftKey ==true) {
					return false;
				} 
			}
			document.onkeydown =keydown;
			function drag() {
				return false;
			}
			document.ondragstart=drag;
			function stopmouse(e) {
				if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
					return false;
				else if	(navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
					return false;
				}
				return true;
			}
			document.onmousedown=stopmouse;
			if (document.layers)
				window.captureEvents(Event.MOUSEDOWN);
			window.onmousedown=stopmouse;
		</script>
	</head>
	<body>
		<?php include 'header.php';?>
		<div class="banner">
            <div class="hd" style="top: 687.5px; display: block;">
                <ul class="clear"><li class="">1</li><li class="">2</li><li class="">3</li><li class="">4</li><li class="">5</li><li class="">6</li><li class="on">7</li><li class="">8</li><li class="">9</li><li class="">10</li></ul>
            </div>
            <?php
                $pics = array();
                $dbNavPic = new DB();
                $sqlPic = "select id, file_path, nav_id from mz_main_pic where state='1' order by order_num limit 10";
                $stmtNavPic = $dbNavPic -> prepare($sqlPic);
                // 处理打算执行的SQL命令
                $stmtNavPic->execute();
                // 执行SQL语句
                $stmtNavPic->store_result();
                // 输出查询的记录个数
                $stmtNavPic->bind_result($id, $filePath, $workId);
                while ($stmtNavPic->fetch())
                {
                    $tmpArr = array();
                    $tmpArr['id'] = $id;
                    $tmpArr['filePath'] = $filePath;
                    $tmpArr['workId'] = $workId;
                    $pics[] = $tmpArr;
                }
            ?>
            <div class="bd" style="height: 727.5px;">
                <ul style="position: relative; width: 1914px; height: 727.5px;">
                    <?php for($i=0; $i<count($pics);$i++) { ?>
                    <li data-bg="<?php echo $pics[$i]['filePath'] ?>" data-load="yes" style="position: absolute; width: 1914px; left: 0px; top: 0px; background-image: url(&quot;<?php echo $pics[$i]['filePath'] ?>&quot;); height: 727.5px; background-size: 1920px 727.5px; display: none;">
                        <a href="/detail.php?id=<?php echo $pics[$i]['workId'] ?>" target="_blank"></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <a class="btn-prev arrow"><span></span></a>
            <a class="btn-next arrow"><span></span></a>
        </div>
        <div class="fullSlide2 slide">
            <div class="bd">
                <ul>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/1533799979_2458.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20171221100056_166.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/1533799813_2864.jpg"></a>
                    </li>
                    <li style="display: list-item;">
                        <a target="_blank" href="#"><img src="./hhhh/1533800243_1858.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20170118140552_828.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20170215155354_772.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20171221101945_775.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20170215155452_594.jpg"></a>
                    </li>
                    <li style="display: none;">
                        <a target="_blank" href="#"><img src="./hhhh/20170215155824_678.jpg"></a>
                    </li>
                </ul>
            </div>
            <div class="hd">
                <ul><li class="">1</li><li class="">2</li><li class="">3</li><li class="on">4</li><li class="">5</li><li class="">6</li><li class="">7</li><li class="">8</li><li class="">9</li></ul>
            </div>
            <a class="prev" href="javascript:void(0)"></a>
            <a class="next" href="javascript:void(0)"></a>
        </div>
		<style type="text/css">
    	    .banner .arrow {position:absolute; top:25%; margin-top:0; width:100px; height:50%; padding:0;cursor: pointer;}
			.banner .arrow span,.banner .arrow span:after,.banner .arrow span:before {position:absolute; top:50%; height:2px; background-color:#fff; border-radius:1px; -webkit-backface-visibility:hidden; backface-visibility:hidden; -webkit-transition:all .4s ease; transition:all .4s ease } 
			.banner .arrow span {margin-top:-1px; width:0 } 
			.banner .arrow span:after,.banner .arrow span:before {content:""; width:30px } 
			.banner .arrow.btn-prev span {left:50%; margin-left:-10px } 
			.banner .arrow.btn-prev span:before {left:0; bottom:0; -webkit-transform:rotate(-45deg); -ms-transform:rotate(-45deg); transform:rotate(-45deg); -webkit-transform-origin:left; -ms-transform-origin:left; transform-origin:left } 
			.banner .arrow.btn-prev span:after {left:0; top:0; -webkit-transform:rotate(45deg); -ms-transform:rotate(45deg); transform:rotate(45deg); -webkit-transform-origin:left; -ms-transform-origin:left; transform-origin:left } 
			.banner .arrow.btn-next span {right:50%; margin-right:-10px } 
			.banner .arrow.btn-next span:before {right:0; bottom:0; -webkit-transform:rotate(45deg); -ms-transform:rotate(45deg); transform:rotate(45deg); -webkit-transform-origin:right; -ms-transform-origin:right; transform-origin:right } 
			.banner .arrow.btn-next span:after {right:0; top:0; -webkit-transform:rotate(-45deg); -ms-transform:rotate(-45deg); transform:rotate(-45deg); -webkit-transform-origin:right; -ms-transform-origin:right; transform-origin:right } 
            .banner .arrow:hover span {width:40px }
            .banner .arrow:hover.btn-prev span {margin-left:-20px }
            .banner .arrow:hover.btn-next span {margin-right:-20px }
            .banner .btn-next {
				right:2.1%
			}
			.banner .btn-prev {
				left:2.1%
			}
		</style>
		<div class="warp1 hd">
			<div class="hd_title">西美品牌—欢迎来到国内领先的品牌策划与创意机构！<br>
				<span>SEEMORE—DOMESTIC LEADING BRAND PLANNING AND CREATIVE ORGANIZATION！</span>
			</div>
		</div>
		<div class="warp2">
			<div class="warp-type">
                <p><a href="/works.php" class="museo-light">SEE   OUR   WORK</a></p>
                <div class="warp-type-list">
                    <a href="/works.php" target="_blank" class="active museo-light">All</a>
                    <a href="/works.php" target="_blank" rel="nofollow">品牌策划</a>
                    <a href="/works.php" target="_blank" rel="nofollow">品牌设计</a>
                    <a href="/works.php" target="_blank" rel="nofollow">LOGO VI设计</a>
                    <a href="/works.php" target="_blank" rel="nofollow">包装策划设计</a>
                </div>
			</div>
			<?php 
				$works = array();
				$dbNav = new DB();
				$sql = "select id, name, name_cn, name_en, image_url from mz_work where state='1' order by order_num limit 12";
				$stmtNav = $dbNav -> prepare($sql);
				// 处理打算执行的SQL命令
				$stmtNav->execute();
				// 执行SQL语句
				$stmtNav->store_result();
				// 输出查询的记录个数
				$stmtNav->bind_result($id, $name, $nameCn, $nameEn, $imageUrl);
				while ($stmtNav->fetch())
				{
					$tmpArr = array();
					$tmpArr['id'] = $id;
					$tmpArr['name'] = $name;
					$tmpArr['nameCn'] = $nameCn;
					$tmpArr['nameEn'] = $nameEn;
					$tmpArr['imageUrl'] = $imageUrl;
					$works[] = $tmpArr;
				} 
			?>
			<ul class="case clear">
				<?php for($i=0; $i<count($works);$i++) { ?>
					<a href="/detail.php?id=<?php echo $works[$i]['id'] ?>" target="_blank">
					<li>
						<img src="<?php echo $works[$i]['imageUrl'] ?>" class="lazy tran imgbig" alt="<?php echo $works[$i]['nameCn'] ?>" data-original="<?php echo $works[$i]['imageUrl'] ?>" style="display: none; transform: scale(1);">
						<div class="case-summary" style="display: none; opacity: 1;">
							<div style="position: relative;width:100%;height: 100%">
								<p class="p1"><?php echo $works[$i]['name'] ?></p>
								<p class="p2">-<br><span class="syl"><?php echo $works[$i]['nameCn'] ?></span>
													<br> <?php echo $works[$i]['nameEn'] ?>                            
								</p>
							</div>
						</div>
				</li>
					</a>
				<?php } ?>		
			</ul>
			<a href="/works.php" class="case-more">MORE<i>&gt;</i></a>
		</div>
		<div style="text-align:center;">
            <a href="/viewpoint.php" style="font-size: 18px;" class="warp3-title">品牌策划观点<span>&gt;</span></a>
        </div>
		<div class="warp3" style="padding-top:10px;width:768px;">
			<div class="warp3-list">
				<ul>
					<li><a href="/news/1038.php" target="_blank">食品包装设计的三个趋势，有必要了解下！</a></li>
					<li><a href="/news/1038.php" target="_blank">产品策划包装设计的三大要点，十分重要！</a></li>
					<li><a href="/news/1038.php" target="_blank">网红包装设计技巧 轻松抓住消费者的目光</a></li>
				</ul>
			</div>
			<div class="warp3-list">
                <ul>
                    <li><a href="/news/1038.php" target="_blank">品牌策划设计中需要遵循的一些法则</a></li>
                    <li><a href="/news/1038.php" target="_blank">饮料包装设计的趋势及技巧</a></li>
                    <li><a href="/news/1038.php" target="_blank">品牌战略咨询具体要做些什么</a></li>
                </ul>
            </div>
        </div>

		<div style="width:100%">
			<script id="jsID" type="text/javascript">
				jQuery("#nav").slide({ type:"menu", titCell:".nLi", targetCell:".sub",effect:"slideDown",delayTime:300,triggerTime:0,defaultPlay:false,returnDefault:true});
			</script>
			<script type="text/javascript">
				$(document).ready(function(){
					$(".side ul li").hover(function(){
						$(this).find(".sidebox").stop().animate({"width":"124px"},200).css({"opacity":"1","filter":"Alpha(opacity=100)","background":"#ae1c1c"})	
						$(this).find(".sidebox2").stop().animate({"width":"174px"},200).css({"opacity":"1","filter":"Alpha(opacity=100)","background":"#ae1c1c"})	
					},function(){
						$(this).find(".sidebox").stop().animate({"width":"50px"},200).css({"opacity":"0.8","filter":"Alpha(opacity=80)","background":"#000"})	
						$(this).find(".sidebox2").stop().animate({"width":"50px"},200).css({"opacity":"0.8","filter":"Alpha(opacity=80)","background":"#000"})	
					});
					
				});
				//回到顶部
				function goTop(){
					$('html,body').animate({'scrollTop':0},600); //滚回顶部的时间，越小滚的速度越快~
				}
			</script>
			<style>
				.side{position:fixed;width:50px;height:275px;right:0;top:410px;z-index:100;}
				.side ul li{width:50px;height:51px;float:left;position:relative; }
				.side ul li .sidebox{position:absolute;width:50px;height:50px;top:0;right:0;transition:all 0.3s;background:#000;opacity:0.8;filter:Alpha(opacity=80);color:#fff;font:14px/50px "微软雅黑";overflow:hidden; border-radius:6px 0 0 6px; z-index:99;}
				.side ul li .sidetop{width:50px;height:50px;line-height:50px;display:inline-block;background:#000;opacity:0.8;filter:Alpha(opacity=80);transition:all 0.3s; border-radius:6px 0 0 6px;}
				.side ul li .sidetop:hover{background:#ae1c1c;opacity:1;filter:Alpha(opacity=100);}
				.side ul li img{float:left;}
				.side ul li .sidebox2{position:absolute;width:50px;height:50px;top:0;right:0;transition:all 0.3s;background:#000;opacity:0.8;filter:Alpha(opacity=80);color:#fff;font:14px/50px "微软雅黑";overflow:hidden; border-radius:6px 0 0 6px;}
				.side ul li .sidetop3{width:50px;height:50px;line-height:50px;display:inline-block;background:#000;opacity:0.8;filter:Alpha(opacity=80);transition:all 0.3s; border-radius:6px 0 0 6px; cursor:pointer; z-index:2;}
				.side ul li .sidetop3:hover{background:#ae1c1c;opacity:1;filter:Alpha(opacity=100);}
				.side ul li .sidetop3:hover .gr{opacity:1; transition:all 0.5s; left:-150px;}
				.gr{ opacity:0; position:absolute; left:150px; bottom:0; transition:all 0.5s; z-index:0;
				.hd ul .on{background: rgba(255,255,5,0.6);}
			</style>
			<div class="side">
				<ul>
					<li><div class="sidebox2"><img src="./images/side_icon01.png" width="50">187-6581-7278</div></li>
					<li>
						<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=444166815&amp;site=qq&amp;menu=yes" target="_blank">
							<div class="sidebox"><img src="./images/side_icon04.png" width="50">QQ客服</div>
						</a>
					</li>
					<li>
						<div class="sidetop3">
							<img src="./images/side_icon03.png" width="50">
							<div class="gr"><img src="./images/weixin_contact.jpg" width="150"></div>
						</div>
					</li>
					<li style="border:none;"><a href="javascript:goTop();" class="sidetop"><img src="./images/side_icon05.png" width="50"></a></li>
				</ul>
			</div>
		</div>
		<?php include './common/foot01.html';?>
	</body>
</html>