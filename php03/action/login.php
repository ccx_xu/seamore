<?php
	include dirname(__FILE__) . '/../common/DB.class.php';
	$arr = array();
	session_start();
	$arr['msg'] = $_REQUEST['validateCode'].$_SESSION['authcode'];
	if (isset($_REQUEST['validateCode']) && strtolower($_REQUEST['validateCode'])==$_SESSION['authcode']) {
		$db = new DB();
		$username = $_REQUEST['username'];
		$password = md5($_REQUEST['password'].'{'.$_REQUEST['username'].'}');
		$sql = "select id, name, username, email, photo_path, user_comment, mobile, department  from mz_user where username = ? and password = ?";
		$stmt = $db -> prepare($sql);
		$stmt->bind_param("ss", $fname, $sname);
	 
		// 设置参数并执行
		$fname = $username;
		$sname = $password;
		// 处理打算执行的SQL命令
		$stmt->execute();
		// 执行SQL语句
		$stmt->store_result();
		// 输出查询的记录个数
	    $stmt->bind_result($id, $fname, $susername, $temail, $fphotoPath, $duty, $mobile, $dept);
		if ($stmt->fetch())
	    {
	    	//  当验证通过后，启动 Session
	    	// session_start();
	    	//  注册登陆成功的 admin 变量，并赋值 true
	    	$_SESSION["admin"] = true;
	    	$_SESSION["id"] = $id;
	    	$_SESSION["name"] = $fname;
	    	$_SESSION["username"] = $username;
	    	$_SESSION["email"] = $temail;
	    	$_SESSION["mobile"] = $mobile;
	    	$_SESSION["headImg"] = $fphotoPath;
	    	$_SESSION["duty"] = $duty;
	    	$_SESSION["dept"] = $dept;
	    	$arr['code'] = 0;
	    } else {
	    	$arr['code'] = 1;
	   		$arr['msg'] = "用户名或密码错误！";
	   	}
	} else {
		$arr['code'] = 1;
	   	//$arr['msg'] = "验证码错误！";
	}
   	echo json_encode($arr);
	
?>