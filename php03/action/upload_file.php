<?php	
	include dirname(__FILE__) . '/../common/Config.class.php';
	$image_path = Config::getconfig("image_path");
	$server_path = Config::getconfig("server_path");

	$result = array();
	// 允许上传的图片后缀
	$allowedExts = array("gif", "jpeg", "jpg", "png");
	$temp = explode(".", $_FILES["imgFile"]["name"]);
	$extension = end($temp);     // 获取文件后缀名
	if ((($_FILES["imgFile"]["type"] == "image/gif")
		|| ($_FILES["imgFile"]["type"] == "image/jpeg")
		|| ($_FILES["imgFile"]["type"] == "image/jpg")
		|| ($_FILES["imgFile"]["type"] == "image/pjpeg")
		|| ($_FILES["imgFile"]["type"] == "image/x-png")
		|| ($_FILES["imgFile"]["type"] == "image/png"))
		&& ($_FILES["imgFile"]["size"] < 20480000)   // 小于 200 kb
		&& in_array($extension, $allowedExts)) {
	    if ($_FILES["imgFile"]["error"] > 0) {
	        $result['error'] = $_FILES["imgFile"]["error"];
	    } else {
	        $result['filename'] = $_FILES["imgFile"]["name"];
	        $result['filetype'] = $_FILES["imgFile"]["type"];
	        $result['filesize'] = ($_FILES["imgFile"]["size"] / 1024) . "kB";
	        $result['filepath'] = $_FILES["imgFile"]["tmp_name"];
	        
	        // 判断当前目录下的 upload 目录是否存在该文件
	        // 如果没有 upload 目录，你需要创建它，upload 目录权限为 777
	        $imgPath = date("Ymd",time());
	        $result['image_path'] = $image_path;
	        $result['imgPath'] = $imgPath;
	        if (is_dir($image_path.$imgPath)) {
				$result['test'] = "001";
	        } else {
	        	$result['test'] = "001".$image_path.$imgPath;
	        	mkdir($image_path.$imgPath, 0777, true);
	        }
	        if (file_exists($image_path.$imgPath."/".$_FILES["imgFile"]["name"])) {
	            $result['error'] =  $_FILES["imgFile"]["name"] . " 文件已经存在。 ";
	        } else {	        	
	            // 如果 upload 目录不存在该文件则将文件上传到 upload 目录下
	            $a =  get_msectime();
	            $filename00 = get_microtime_format($a*0.001);
	            move_uploaded_file($_FILES["imgFile"]["tmp_name"], $image_path.$imgPath."/".$filename00.".".$extension);
	            $result['msg'] =  "文件存储在: " . "/works/".$imgPath."/".$filename00.".".$extension;
	            $result['src'] = "/works/".$imgPath."/".$filename00.".".$extension;
	        }
	    }
	} else  {
	    $result['error'] =  "非法的文件格式";
	}
	echo json_encode($result);

	//返回当前的毫秒时间戳
    function get_msectime() {
        list($msec, $sec) = explode(' ', microtime());
        $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    /** 
      *时间戳 转   日期格式 ： 精确到毫秒，x代表毫秒
      */
    function get_microtime_format($time) {  
        if(strstr($time,'.')){
            sprintf("%01.3f",$time); //小数点。不足三位补0
            list($usec, $sec) = explode(".",$time);
            $sec = str_pad($sec,3,"0",STR_PAD_RIGHT); //不足3位。右边补0
        }else{
            $usec = $time;
            $sec = "000"; 
        }
        $date = date("YmdHisx",$usec);
        return str_replace('x', $sec, $date);
    }

?>