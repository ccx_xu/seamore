<?php 
	include dirname(__FILE__) . '/../common/DB.class.php';
	include dirname(__FILE__) . '/../common/pageInfo.class.php';

	class User {
		var $id = "";
		var $name = "";
		var $username = "";
		var $email = "";
		var $mobile = "";
		var $userComment = "";
		var $fax = "";
		var $photoPath = "";
		var $tele = "";
		var $status = "";

		function __construct() {
   		}

   		function __destruct() {
       		
   		}

		function getId() {
			echo is_null($this->id)?"":$this->id;
		}

		function setId($id) {
			$this->id = $id;
		}

		function getName() {
			echo $this->name;
		}

		function setName($name) {
			$this->name = $name;
		}

		function getUsername() {
			echo $this->username;
		}

		function setUsername($username) {
			$this->username = $username;
		}

		function getEmail() {
			echo $this->email;
		}

		function setEmail($email) {
			$this->email = $email;
		}

		function getMobile() {
			echo $this->mobile;
		}

		function setMobile($mobile) {
			$this->mobile = $mobile;
		}

		function getUserComment() {
			echo $this->userComment;
		}

		function setUserComment($userComment) {
			$this->userComment = $userComment;
		}

		function getFax() {
			echo $this->fax;
		}

		function setFax($fax) {
			$this->fax = $fax;
		}

		function getPhotoPath() {
			echo $this->photoPath;
		}

		function setPhotoPath($photoPath) {
			$this->photoPath = $photoPath;
		}

		function getTele() {
			echo $this->tele;
		}

		function setTele($tele) {
			$this->tele = $tele;
		}

		function getStatus() {
			echo $this->status .PHP_EOL;
		}

		function setStatus($status) {
			$this->status = $status;
		}
	}

	$arr = array();
	$db = new DB();
	if(isset($_REQUEST['method']) && !empty($_REQUEST['method'])) {
		if ("list" === $_REQUEST['method']) {
			$pageInfo = new PageInfo();
			$sql = "select id, name, username, email, photo_path from mz_user";
			$sqlCount = "select count(*) from mz_user";
			$stmt = $db -> prepare($sql);
			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($id, $fname, $username, $temail, $fphotoPath);
		    $index = 0;
			while ($stmt->fetch())
		    {
		    	$user = new User();
		    	$user->setId($id);
		    	$user->setName($fname);
		    	$user->setUsername($username);
		    	$user->setPhotoPath($fphotoPath);
		    	$arr[$index] = $user;
		    	$index++;
		    } 

		    $stmt0 = $db -> prepare($sqlCount);
			// 处理打算执行的SQL命令
			$stmt0->execute();
			// 执行SQL语句
			$stmt0->store_result();
			// 输出查询的记录个数
		    $stmt0->bind_result($count);
		    if ($stmt0->fetch())
		    {
		    	$pageInfo->setTotal($count);
		    } 
		    
		    $pageInfo->setRows($arr);
		    
		    echo json_encode($pageInfo);
		} else if ("add" === $_REQUEST['method']) {
			$name = $_POST['name'];
			$username = $_POST['username'];
			$email = $_POST['email'];
			$tele = $_POST['tele'];
			$fax = $_POST['fax'];
			$mobile = $_POST['mobile'];
			$status = $_POST['status'];

			$sql = "insert into mz_user(id, name, username, email, tele, fax, mobile, status, pwd, password, photo_path) values(?,?,?,?,?,?,?,?,?,?,?)";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("sssssssssss", $p01, $p02, $p03, $p04, $p05, $p06, $p07, $p08, $p09, $p10, $p11);
		 
			// 设置参数并执行
			$p01 = $db->uuid();
			$p02 = $name;
			$p03 = $username;
			$p04 = $email;
			$p05 = $tele;
			$p06 = $fax;
			$p07 = $mobile;
			$p08 = $status;
			$p09 = "123456";
			$p10 = md5("123456{".$username."}");
			$p11 = "/img/head.jpg";
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("edit" === $_REQUEST['method']) {
			$id = $_POST['id'];
			$name = $_POST['name'];
			$username = $_POST['username'];
			$email = $_POST['email'];
			$tele = $_POST['tele'];
			$fax = $_POST['fax'];
			$mobile = $_POST['mobile'];
			$status = $_POST['status'];

			$sql = "update mz_user set name=?, username=?, email=?, tele=?, fax=?, mobile=?, status=? where id = ?";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ssssssss", $p02, $p03, $p04, $p05, $p06, $p07, $p08, $p01);
		 
			// 设置参数并执行
			$p01 = $id;
			$p02 = $name;
			$p03 = $username;
			$p04 = $email;
			$p05 = $tele;
			$p06 = $fax;
			$p07 = $mobile;
			$p08 = $status;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("saveprop" === $_REQUEST['method']) {
			session_start();
			$id = $_SESSION["id"];
			$name = $_POST['name'];
			$username = $_POST['username'];
			$email = $_POST['email'];
			$title = $_POST['title'];
			$dept = $_POST['dept'];
			$mobile = $_POST['mobile'];

			$sql = "update mz_user set name=?, username=?, email=?, department=?, user_comment=?, mobile=? where id = ?";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("sssssss", $p02, $p03, $p04, $p05, $p06, $p07, $p01);
		 
			// 设置参数并执行
			$p01 = $id;
			$p02 = $name;
			$p03 = $username;
			$p04 = $email;
			$p05 = $dept;
			$p06 = $title;
			$p07 = $mobile;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = "修改成功！重新登录后生效.";
			echo json_encode($arr);
		} else if ("savepwd" === $_REQUEST['method']) {
			session_start();
			$id = $_SESSION["id"];
			$npwd = $_POST['newPassword'];

			$sql = "update mz_user set pwd=? where id = ?";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ss", $p02, $p01);
		 
			// 设置参数并执行
			$p01 = $id;
			$p02 = $npwd;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = "修改成功！重新登录后生效.";
			echo json_encode($arr);
		} else if ("del" === $_REQUEST['method']) {
			$ids = $_POST['ids'];
			$str_arr = explode(",",$ids);

    		for($i=0; $i<count($str_arr); $i++){
				$sql = "delete from mz_user where id = ?";
				$stmt = $db -> prepare($sql);
				$stmt->bind_param("s", $p01);
				// 设置参数并执行
				$p01 = $str_arr[$i];
				// 处理打算执行的SQL命令
				$stmt->execute();
			}			

			$arr['code'] = 0;
			$arr['msg'] = "删除成功！！";
			echo json_encode($arr);
		} else if ("checkUsernameUnique" === $_REQUEST['method']) {
			$username = $_REQUEST['username'];
			session_start();
			$id = $_SESSION["id"];

			$sql = "select count(1) from mz_user where username = ? and id != ?";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ss", $fname, $sid);
	 
			// 设置参数并执行
			$fname = $username;
			$sid = $id;
			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($count);
		    $total = 0;
			if ($stmt->fetch()) {
				$total = $count;
			}
			echo ($total>0?'false':'true');
			//echo json_encode($arr);
		} else if ("checkPassword" === $_REQUEST['method']) {
			$password = $_REQUEST['password'];
			session_start();
			$id = $_SESSION["id"];

			$sql = "select username, password from mz_user where id = ?";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("s", $sid);
			$sid = $id;
			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($rusername, $rpassword);
		    $r1 = "";
		    $r2 = "";
			if ($stmt->fetch()) {
				$r1 = $rusername;
				$r2 = $rpassword;
			}
			$input = md5($password."{".$r1."}");
			echo ($input===$r2?'true':'false');
			//echo json_encode($arr);
		} else if ("upload" === $_REQUEST['method']) {
			$result = array();
			// 允许上传的图片后缀
			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$id = $_POST["id"];
			$temp = explode(".", $_FILES["imgFile"]["name"]);
			$extension = end($temp);     // 获取文件后缀名
			if ((($_FILES["imgFile"]["type"] == "image/gif")
				|| ($_FILES["imgFile"]["type"] == "image/jpeg")
				|| ($_FILES["imgFile"]["type"] == "image/jpg")
				|| ($_FILES["imgFile"]["type"] == "image/pjpeg")
				|| ($_FILES["imgFile"]["type"] == "image/x-png")
				|| ($_FILES["imgFile"]["type"] == "image/png"))
				&& ($_FILES["imgFile"]["size"] < 20480000)   // 小于 200 kb
				&& in_array($extension, $allowedExts)) {
			    if ($_FILES["imgFile"]["error"] > 0) {
			        $result['error'] = $_FILES["imgFile"]["error"];
			    } else {
			        $result['filename'] = $_FILES["imgFile"]["name"];
			        $result['filetype'] = $_FILES["imgFile"]["type"];
			        $result['filesize'] = ($_FILES["imgFile"]["size"] / 1024) . "kB";
			        $result['filepath'] = $_FILES["imgFile"]["tmp_name"];
			        
			        // 判断当前目录下的 upload 目录是否存在该文件
			        // 如果没有 upload 目录，你需要创建它，upload 目录权限为 777
			        
			        if (file_exists("../img/".$_FILES["imgFile"]["name"])) {
			            $result['error'] =  $_FILES["imgFile"]["name"] . " 文件已经存在。 ";
			        } else {	        	
			            // 如果 upload 目录不存在该文件则将文件上传到 upload 目录下
			            $a =  $db->get_msectime();
			            $filename00 = $db->get_microtime_format($a*0.001);
			            move_uploaded_file($_FILES["imgFile"]["tmp_name"], "../img/".$filename00.".".$extension);

			            $sql = "update mz_user set photo_path=? where id = ?";
						$stmt = $db -> prepare($sql);
						$stmt->bind_param("ss", $p01, $p02);
					 
						// 设置参数并执行
						$p01 = "/img/".$filename00.".".$extension;
						$p02 = $id;
						// 处理打算执行的SQL命令
						$stmt->execute();

			            $result['code'] = 0;
			            $result['msg'] =  "头像修改成功，重新登录后生效!!!!";
			        }
			    }
			} else  {
			    $result['error'] =  "非法的文件格式";
			}
			echo json_encode($result);
		}
    //echo $arr;
	}
?>