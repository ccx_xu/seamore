<?php 
	include dirname(__FILE__) . '/../common/DB.class.php';
	include dirname(__FILE__) . '/../common/pageInfo.class.php';

	class Navigation {

		var $id = "";
		var $nameEn = "";
		var $name = "";
		var $orderNum = 0;
		var $state = "1";
		var $staticUrl = "";

		function __construct() {

		}

		function __destruct() {

		}

		function getId() {
			echo $this->id;
		}

		function setId($id) {
			$this->id = $id;
		}

		function getNameEn() {
			echo $this->nameEn;
		}

		function setNameEn($nameEn) {
			$this->nameEn = $nameEn;
		}

		function getName() {
			echo $this->name;
		}

		function setName($name) {
			$this->name = $name;
		}

		function getOrderNum() {
			echo $this->orderNum;
		}

		function setOrderNum($orderNum) {
			$this->orderNum = $orderNum;
		}

		function getState() {
			echo $this->state;
		}

		function setState($state) {
			$this->state = $state;
		}

		function getStaticUrl() {
			echo $this->staticUrl;
		}

		function setStaticUrl($staticUrl) {
			$this->staticUrl = $staticUrl;
		}
	
	}

	function getParamTypeStr($arr){
		$count = count($arr);
		$typestr = "";
		for($i = 0; $i<$count; $i++){
			$type = gettype($arr[$i]);
			switch($type){
				case "integer":
					$typestr.= "i";
					break;
				case "float":
				case "double":
					$typestr.= "d";
					break;
				case "string":
					$typestr.= "s";
					break;
			}
		}
		return $typestr;
	}


	function refValues($arr){
		if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}


	$arr = array();
	$db = new DB();
	if(isset($_REQUEST['method']) && !empty($_REQUEST['method'])) {
		if ("list" === $_REQUEST['method']) {
			$pageInfo = new PageInfo();
			$args = array();
			$argsCount = array();

			$sql = "select id, name_en, name, order_num, state, static_url from mz_navigation ";
			$sqlCount = "select count(*) from mz_navigation";
			$sql .= " where 1=1 ";
			$sqlCount .= " where 1=1 ";
			if(isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
				$sql .= " and name like ? ";
				$sqlCount .= " and name like ? ";
				$args[] = "%".$_REQUEST['name']."%";
				$argsCount[] = "%".$_REQUEST['name']."%";
			}
			$sql .= " limit ?, ?";
			$stmt = $db -> prepare($sql);

			$callback = array($stmt, 'bind_param');
			// 将参数类型描述加入数组
			
			$args[] = $pageInfo->start;
			$args[] = $pageInfo->pageSize;

			array_unshift($args, getParamTypeStr($args)); 
			//call_user_func_array($callback, $args);

			call_user_func_array($callback, refValues($args));

			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($id, $nameEn, $name, $orderNum, $state, $staticUrl);
		    $index = 0;
			while ($stmt->fetch())
		    {
		    	$nav = new Navigation();
		    	$nav->setId($id);
		    	$nav->setNameEn($nameEn);
		    	$nav->setName($name);
		    	$nav->setOrderNum($orderNum);
		    	$nav->setState($state);
		    	$nav->setStaticUrl($staticUrl);
		    	$arr[$index] = $nav;
		    	$index++;
		    } 

		    $stmt0 = $db -> prepare($sqlCount);

		    if(count($argsCount)>0) {
			    $callback0 = array($stmt0, 'bind_param');
				array_unshift($argsCount, getParamTypeStr($argsCount)); 
				//call_user_func_array($callback, $args);
				call_user_func_array($callback0, refValues($argsCount));
			}

			// 处理打算执行的SQL命令
			$stmt0->execute();
			// 执行SQL语句
			$stmt0->store_result();
			// 输出查询的记录个数
		    $stmt0->bind_result($count);
		    if ($stmt0->fetch())
		    {
		    	$pageInfo->setTotal($count);
		    } 

		    $pageInfo->setRows($arr);
		    
		    echo json_encode($pageInfo);

		} else if ("add" === $_REQUEST['method']) {
			$nameEn = $_POST['nameEn'];
			$name = $_POST['name'];
			$staticUrl = $_POST['staticUrl'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];
			$sql = "insert into mz_navigation(id, name_en, name, static_url, order_num, state) values(?,?,?,?,?,?)";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ssssis", $p1, $p2, $p3, $p4, $p5, $p6);
		 
			// 设置参数并执行
			//echo $db->uuid()."<br/>";
			$p1 = $db->uuid();
			$p2 = $nameEn;
			$p3 = $name;
			$p4 = $staticUrl;
			$p5 = $orderNum;
			$p6 = $state;
			//echo $p1.",".$p2.",".$p3.",".$p4.",".$p5.",".$p6;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("edit" === $_REQUEST['method']) {
			$id = $_POST['id'];
			$nameEn = $_POST['nameEn'];
			$name = $_POST['name'];
			$staticUrl = $_POST['staticUrl'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];
			$sql = "update mz_navigation set name_en=?,name=?,static_url=?,order_num=?,state=? where id=?"; 
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("sssiss", $p1, $p2, $p3, $p4, $p5, $p6);
		 
			// 设置参数并执行
			$p1 = $nameEn;
			$p2 = $name;
			$p3 = $staticUrl;
			$p4 = $orderNum;
			$p5 = $state;
			$p6 = $id;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("del" === $_REQUEST['method']) {
			$ids = $_POST['ids'];
			$str_arr = explode(",",$ids);

    		for($i=0; $i<count($str_arr); $i++){
				$sql = "delete from mz_navigation where id in (?)"; 
				$stmt = $db -> prepare($sql);
				$stmt->bind_param("s", $p1);
				// 设置参数并执行
				$p1 = $str_arr[$i];
				// 处理打算执行的SQL命令
				$stmt->execute();
			}

			$arr['code'] = 0;
			echo json_encode($arr);
		}

	}
?>