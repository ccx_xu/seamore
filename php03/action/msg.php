<?php 
	include dirname(__FILE__) . '/../common/DB.class.php';
	include dirname(__FILE__) . '/../common/pageInfo.class.php';

	class Message {

		var $id = "";
		var $name = "";
		var $gsname = "";
		var $phone = "";
		var $email = "";
		var $cont = "";
		var $subTime = "";

		function __construct() {

		}

		function __destruct() {

		}

		function getId() {
			echo $this->id;
		}

		function setId($id) {
			$this->id = $id;
		}

		function getName() {
			echo $this->name;
		}

		function setName($name) {
			$this->name = $name;
		}

		function getGsname() {
			echo $this->gsname;
		}

		function setGsname($gsname) {
			$this->gsname = $gsname;
		}

		function getPhone() {
			echo $this->phone;
		}

		function setPhone($phone) {
			$this->phone = $phone;
		}

		function getEmail() {
			echo $this->email;
		}

		function setEmail($email) {
			$this->email = $email;
		}


		function getCont() {
			echo $this->cont;
		}

		function setCont($cont) {
			$this->cont = $cont;
		}

		function getSubTime() {
			echo $this->subTime;
		}

		function setSubTime($subTime) {
			$this->subTime = $subTime;
		}
	
	}

	function getParamTypeStr($arr){
		$count = count($arr);
		$typestr = "";
		for($i = 0; $i<$count; $i++){
			$type = gettype($arr[$i]);
			switch($type){
				case "integer":
					$typestr.= "i";
					break;
				case "float":
				case "double":
					$typestr.= "d";
					break;
				case "string":
					$typestr.= "s";
					break;
			}
		}
		return $typestr;
	}


	function refValues($arr){
		if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}


	$arr = array();
	$db = new DB();
	
	$pageInfo = new PageInfo();
	$args = array();
	$argsCount = array();

	$sql = "select id, name, gsname, phone, email, cont, sub_time from mz_message ";
	$sqlCount = "select count(*) from mz_message";
	$sql .= " where 1=1 ";
	$sqlCount .= " where 1=1 ";
	if(isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
		$sql .= " and name like ? ";
		$sqlCount .= " and name like ? ";
		$args[] = "%".$_REQUEST['name']."%";
		$argsCount[] = "%".$_REQUEST['name']."%";
	}
	$sql .= " limit ?, ?";
	$stmt = $db -> prepare($sql);

	$callback = array($stmt, 'bind_param');
	// 将参数类型描述加入数组
	
	$args[] = $pageInfo->start;
	$args[] = $pageInfo->pageSize;

	array_unshift($args, getParamTypeStr($args)); 
	//call_user_func_array($callback, $args);

	call_user_func_array($callback, refValues($args));

	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($id, $name, $gsname, $phone, $email, $cont, $sub_time);
    $index = 0;
	while ($stmt->fetch())
    {
    	$msg = new Message();
    	$msg->setId($id);
    	$msg->setName($name);
    	$msg->setGsname($gsname);
    	$msg->setPhone($phone);
    	$msg->setEmail($email);
    	$msg->setCont($cont);
    	$msg->setSubTime($sub_time);
    	$arr[$index] = $msg;
    	$index++;
    } 

    $stmt0 = $db -> prepare($sqlCount);

    if(count($argsCount)>0) {
	    $callback0 = array($stmt0, 'bind_param');
		array_unshift($argsCount, getParamTypeStr($argsCount)); 
		//call_user_func_array($callback, $args);
		call_user_func_array($callback0, refValues($argsCount));
	}

	// 处理打算执行的SQL命令
	$stmt0->execute();
	// 执行SQL语句
	$stmt0->store_result();
	// 输出查询的记录个数
    $stmt0->bind_result($count);
    if ($stmt0->fetch())
    {
    	$pageInfo->setTotal($count);
    } 

    $pageInfo->setRows($arr);
    
    echo json_encode($pageInfo);

		
?>