<?php 
	include dirname(__FILE__) . '/../common/DB.class.php';
	include dirname(__FILE__) . '/../common/pageInfo.class.php';

	class Category {

		var $id = "";
		var $idKey = "";
		var $name = "";
		var $descc = "";
		var $orderNum = 0;
		var $state = "1";

		function __construct() {

		}

		function __destruct() {

		}

		function getId() {
			echo $this->id;
		}

		function setId($id) {
			$this->id = $id;
		}

		function getIdKey() {
			echo $this->idKey;
		}

		function setIdKey($idKey) {
			$this->idKey = $idKey;
		}

		function getName() {
			echo $this->name;
		}

		function setName($name) {
			$this->name = $name;
		}

		function getDescc() {
			echo $this->descc;
		}

		function setDescc($descc) {
			$this->descc = $descc;
		}

		function getOrderNum() {
			echo $this->orderNum;
		}

		function setOrderNum($orderNum) {
			$this->orderNum = $orderNum;
		}

		function getState() {
			echo $this->state;
		}

		function setState($state) {
			$this->state = $state;
		}
	
	}

	function getParamTypeStr($arr){
		$count = count($arr);
		$typestr = "";
		for($i = 0; $i<$count; $i++){
			$type = gettype($arr[$i]);
			switch($type){
				case "integer":
					$typestr.= "i";
					break;
				case "float":
				case "double":
					$typestr.= "d";
					break;
				case "string":
					$typestr.= "s";
					break;
			}
		}
		return $typestr;
	}


	function refValues($arr){
		if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}


	$arr = array();
	$db = new DB();
	if(isset($_REQUEST['method']) && !empty($_REQUEST['method'])) {
		if ("list" === $_REQUEST['method']) {
			$pageInfo = new PageInfo();
			$args = array();
			$argsCount = array();

			$sql = "select id, id_key, name, cate_desc, order_num, state from mz_category ";
			$sqlCount = "select count(*) from mz_category";
			$sql .= " where 1=1 ";
			$sqlCount .= " where 1=1 ";
			if(isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
				$sql .= " and name like ? ";
				$sqlCount .= " and name like ? ";
				$args[] = "%".$_REQUEST['name']."%";
				$argsCount[] = "%".$_REQUEST['name']."%";
			}
			$sql .= " limit ?, ?";
			$stmt = $db -> prepare($sql);

			/*
			$stmt->bind_param("ii", $start, $psize);
			// 设置参数并执行
			$start = $pageInfo->start;
			$psize = $pageInfo->pageSize;
			*/

			$callback = array($stmt, 'bind_param');
			// 将参数类型描述加入数组
			
			$args[] = $pageInfo->start;
			$args[] = $pageInfo->pageSize;

			array_unshift($args, getParamTypeStr($args)); 
			//call_user_func_array($callback, $args);

			call_user_func_array($callback, refValues($args));

			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($id, $key, $name, $descc, $orderNum, $state);
		    $index = 0;
			while ($stmt->fetch())
		    {
		    	$cate = new Category();
		    	$cate->setId($id);
		    	$cate->setIdKey($key);
		    	$cate->setName($name);
		    	$cate->setDescc($descc);
		    	$cate->setOrderNum($orderNum);
		    	$cate->setState($state);
		    	$arr[$index] = $cate;
		    	$index++;
		    } 

		    $stmt0 = $db -> prepare($sqlCount);

		    if(count($argsCount)>0) {
			    $callback0 = array($stmt0, 'bind_param');
				array_unshift($argsCount, getParamTypeStr($argsCount)); 
				//call_user_func_array($callback, $args);
				call_user_func_array($callback0, refValues($argsCount));
			}

			// 处理打算执行的SQL命令
			$stmt0->execute();
			// 执行SQL语句
			$stmt0->store_result();
			// 输出查询的记录个数
		    $stmt0->bind_result($count);
		    if ($stmt0->fetch())
		    {
		    	$pageInfo->setTotal($count);
		    } 

		    $pageInfo->setRows($arr);
		    
		    echo json_encode($pageInfo);

		} else if ("add" === $_REQUEST['method']) {
			$key = $_POST['key'];
			$name = $_POST['name'];
			$descc = $_POST['descc'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];
			$sql = "insert into mz_category(id, id_key, name, cate_desc, order_num, state) values(?,?,?,?,?,?)";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ssssis", $p1, $p2, $p3, $p4, $p5, $p6);
		 
			// 设置参数并执行
			$p1 = $key;
			$p2 = $key;
			$p3 = $name;
			$p4 = $descc;
			$p5 = $orderNum;
			$p6 = $state;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("edit" === $_REQUEST['method']) {
			$id = $_POST['id'];
			$key = $_POST['key'];
			$name = $_POST['name'];
			$descc = $_POST['descc'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];
			$sql = "update mz_category set id_key=?,name=?,cate_desc=?,order_num=?,state=? where id=?"; 
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("sssiss", $p1, $p2, $p3, $p4, $p5, $p6);
		 
			// 设置参数并执行
			$p1 = $key;
			$p2 = $name;
			$p3 = $descc;
			$p4 = $orderNum;
			$p5 = $state;
			$p6 = $id;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("del" === $_REQUEST['method']) {
			$ids = $_POST['ids'];
			$str_arr = explode(",",$ids);

    		for($i=0; $i<count($str_arr); $i++){
				$sql = "delete from mz_category where id in (?)"; 
				$stmt = $db -> prepare($sql);
				$stmt->bind_param("s", $p1);
				// 设置参数并执行
				$p1 = $str_arr[$i];
				// 处理打算执行的SQL命令
				$stmt->execute();
			}

			$arr['code'] = 0;
			echo json_encode($arr);
		}

	}
?>