<?php 
	include dirname(__FILE__) . '/../common/DB.class.php';
	include dirname(__FILE__) . '/../common/pageInfo.class.php';
	include dirname(__FILE__) . '/../common/Config.class.php';

	$image_path = Config::getconfig("image_path");

	class Work {

		var $id = "";
		var $name = "";
		var $nameCn = "";
		var $nameEn = "";
		var $cateId = "";
		var $imageUrl = "";
		var $cname = "";
		var $sdate = "";
		var $sname = "";
		var $descc = "";
		var $orderNum = 0;
		var $state = "1";

		function __construct() {

		}

		function __destruct() {

		}

		function getId() {
			echo $this->id;
		}

		function setId($id) {
			$this->id = $id;
		}

		function getName() {
			echo $this->name;
		}

		function setName($name) {
			$this->name = $name;
		}

		function getNameCn() {
			echo $this->nameCn;
		}

		function setNameCn($nameCn) {
			$this->nameCn = $nameCn;
		}

		function getNameEn() {
			echo $this->nameEn;
		}

		function setNameEn($nameEn) {
			$this->nameEn = $nameEn;
		}

		function getCateId() {
			echo $this->cateId;
		}

		function setCateId($cateId) {
			$this->cateId = $cateId;
		}

		function getImageUrl() {
			echo $this->imageUrl;
		}

		function setImageUrl($imageUrl) {
			$this->imageUrl = $imageUrl;
		}

		function getCname() {
			echo $this->cname;
		}

		function setCname($cname) {
			$this->cname = $cname;
		}

		function getSdate() {
			echo $this->sdate;
		}

		function setSdate($sdate) {
			$this->sdate = $sdate;
		}

		function getSname() {
			echo $this->sname;
		}

		function setSname($sname) {
			$this->sname = $sname;
		}

		function getDescc() {
			echo $this->descc;
		}

		function setDescc($descc) {
			$this->descc = $descc;
		}

		function getOrderNum() {
			echo $this->orderNum;
		}

		function setOrderNum($orderNum) {
			$this->orderNum = $orderNum;
		}

		function getState() {
			echo $this->state;
		}

		function setState($state) {
			$this->state = $state;
		}
	
	}

	function getParamTypeStr($arr){
		$count = count($arr);
		$typestr = "";
		for($i = 0; $i<$count; $i++){
			$type = gettype($arr[$i]);
			switch($type){
				case "integer":
					$typestr.= "i";
					break;
				case "float":
				case "double":
					$typestr.= "d";
					break;
				case "string":
					$typestr.= "s";
					break;
			}
		}
		return $typestr;
	}


	function refValues($arr){
		if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
			$refs = array();
			foreach($arr as $key => $value)
				$refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}

	$arr = array();
	$db = new DB();
	if(isset($_REQUEST['method']) && !empty($_REQUEST['method'])) {
		if ("list" === $_REQUEST['method']) {
			$pageInfo = new PageInfo();
			$args = array();
			$argsCount = array();

			$sql = "select id, name, name_cn, name_en, (select name from mz_category where id=mw.cate_id) as cate_name, image_url, ".
					" c_name, s_date, s_name, order_num, state from mz_work mw";
			$sqlCount = "select count(*) from mz_work";
			$sql .= " where 1=1 ";
			$sqlCount .= " where 1=1 ";
			if(isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
				$sql .= " and name like ? ";
				$sqlCount .= " and name like ? ";
				$args[] = "%".$_REQUEST['name']."%";
				$argsCount[] = "%".$_REQUEST['name']."%";
			}
			if(isset($_REQUEST['nameCn']) && !empty($_REQUEST['nameCn'])) {
				$sql .= " and name_cn like ? ";
				$sqlCount .= " and name_cn like ? ";
				$args[] = "%".$_REQUEST['nameCn']."%";
				$argsCount[] = "%".$_REQUEST['nameCn']."%";
			}
			if(isset($_REQUEST['state']) && ($_REQUEST['state']==="1" || $_REQUEST['state']==="0")) {
				$sql .= " and state = ? ";
				$sqlCount .= " and state = ? ";
				$args[] = $_REQUEST['state'];
				$argsCount[] = $_REQUEST['state'];
			}
			$sql .= " limit ?, ?";
			$stmt = $db -> prepare($sql);

			/*
			$stmt->bind_param("ii", $start, $psize);
			// 设置参数并执行
			$start = $pageInfo->start;
			$psize = $pageInfo->pageSize;
			*/

			$callback = array($stmt, 'bind_param');
			// 将参数类型描述加入数组
			
			$args[] = $pageInfo->start;
			$args[] = $pageInfo->pageSize;

			array_unshift($args, getParamTypeStr($args)); 
			//call_user_func_array($callback, $args);

			call_user_func_array($callback, refValues($args));

			// 处理打算执行的SQL命令
			$stmt->execute();
			// 执行SQL语句
			$stmt->store_result();
			// 输出查询的记录个数
		    $stmt->bind_result($id, $name, $nameCn, $nameEn, $cateId, $imageUrl, $cname, $sdate, $sname, $orderNum, $state);
		    $index = 0;
			while ($stmt->fetch())
		    {
		    	$work = new Work();
		    	$work->setId($id);
		    	$work->setName($name);
		    	$work->setNameCn($nameCn);
		    	$work->setNameEn($nameEn);
		    	$work->setCateId($cateId);
		    	$work->setImageUrl($imageUrl);
		    	$work->setCname($cname);
		    	$work->setSdate($sdate);
		    	$work->setSname($sname);
		    	$work->setOrderNum($orderNum);
		    	$work->setState($state);
		    	$arr[$index] = $work;
		    	$index++;
		    } 

		    $stmt0 = $db -> prepare($sqlCount);

		    if(count($argsCount)>0) {
			    $callback0 = array($stmt0, 'bind_param');
				array_unshift($argsCount, getParamTypeStr($argsCount)); 
				//call_user_func_array($callback, $args);
				call_user_func_array($callback0, refValues($argsCount));
			}

			// 处理打算执行的SQL命令
			$stmt0->execute();
			// 执行SQL语句
			$stmt0->store_result();
			// 输出查询的记录个数
		    $stmt0->bind_result($count);
		    if ($stmt0->fetch())
		    {
		    	$pageInfo->setTotal($count);
		    } 

		    $pageInfo->setRows($arr);
		    
		    echo json_encode($pageInfo);

		} else if ("add" === $_REQUEST['method']) {
			$name = $_POST['name'];
			$nameCn = $_POST['nameCn'];
			$nameEn = $_POST['nameEn'];
			$cateId = $_POST['cateId'];
			$imageUrl = $_POST['imageUrl_'];
			$cname = $_POST['cname'];
			$sname = $_POST['sname'];
			$sdate = $_POST['sdate'];
			$descc = $_POST['htmlContent'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];

			$imgPath = date("Ymd",time()); 
	        if (is_dir($image_path.$imgPath)) {

	        } else {
	        	mkdir($image_path.$imgPath, 0777, true);
	        }
	        $a =  $db->get_msectime();
	        $filename00 = $db->get_microtime_format($a*0.001);
			$myfile = fopen($image_path.$imgPath."/".$filename00.".html", "w") or die("Unable to open file!");
			fwrite($myfile, $descc);
			fclose($myfile);

			$sql = "insert into mz_work(id, name, name_cn, name_en, cate_id, image_url, order_num, c_name, s_date, s_name, descc, state) values(?,?,?,?,?,?,?,?,?,?,?,?)";
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ssssssisssss", $p01, $p02, $p03, $p04, $p05, $p06, $p07, $p08, $p09, $p10, $p11, $p12);
		 
			// 设置参数并执行
			$p01 = $db->uuid();
			$p02 = $name;
			$p03 = $nameCn;
			$p04 = $nameEn;
			$p05 = $cateId;
			$p06 = $imageUrl;
			$p07 = $orderNum;
			$p08 = $cname;
			$p09 = $sdate;
			$p10 = $sname;
			$p11 = "/works/".$imgPath."/".$filename00.".html";
			$p12 = $state;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("edit" === $_REQUEST['method']) {
			$id = $_POST['id'];
			$name = $_POST['name'];
			$nameCn = $_POST['nameCn'];
			$nameEn = $_POST['nameEn'];
			$cateId = $_POST['cateId'];
			$imageUrl = $_POST['imageUrl_'];
			$cname = $_POST['cname'];
			$sname = $_POST['sname'];
			$sdate = $_POST['sdate'];
			$descc = $_POST['htmlContent'];
			$orderNum = $_POST['orderNum'];
			$state = $_POST['state'];

			$imgPath = date("Ymd",time()); 
	        if (is_dir($image_path.$imgPath)) {

	        } else {
	        	mkdir($image_path.$imgPath, 0777, true);
	        }
	        $a =  $db->get_msectime();
	        $filename00 = $db->get_microtime_format($a*0.001);
			$myfile = fopen($image_path.$imgPath."/".$filename00.".html", "w") or die("Unable to open file!");
			fwrite($myfile, $descc);
			fclose($myfile);

			$sql = "update mz_work set name=?, name_cn=?, name_en=?, cate_id=?, image_url=?, order_num=?, c_name=?, s_date=?, s_name=?, descc=?, state=? where id=?"; 
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("sssssissssss", $p02, $p03, $p04, $p05, $p06, $p07, $p08, $p09, $p10, $p11, $p12, $p01);
		 
			// 设置参数并执行
			$p01 = $id;
			$p02 = $name;
			$p03 = $nameCn;
			$p04 = $nameEn;
			$p05 = $cateId;
			$p06 = $imageUrl;
			$p07 = $orderNum;
			$p08 = $cname;
			$p09 = $sdate;
			$p10 = $sname;
			$p11 = "/works/".$imgPath."/".$filename00.".html";
			$p12 = $state;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = $name;
			echo json_encode($arr);
		} else if ("save" === $_REQUEST['method']) {
			$id = $_POST['id'];
			$state = $_POST['state'];
			$sql = "update mz_work set state=? where id=?"; 
			$stmt = $db -> prepare($sql);
			$stmt->bind_param("ss", $p02, $p01);
		 
			// 设置参数并执行
			$p01 = $id;
			$p02 = $state;
			// 处理打算执行的SQL命令
			$stmt->execute();

			$arr['code'] = 0;
			$arr['msg'] = "修改成功！";
			echo json_encode($arr);
		} else if ("del" === $_REQUEST['method']) {
			$ids = $_POST['ids'];
			$str_arr = explode(",",$ids);

    		for($i=0; $i<count($str_arr); $i++){
				$sql = "delete from mz_work where id in (?)"; 
				$stmt = $db -> prepare($sql);
				$stmt->bind_param("s", $p1);
				// 设置参数并执行
				$p1 = $str_arr[$i];
				// 处理打算执行的SQL命令
				$stmt->execute();
			}

			$arr['code'] = 0;
			echo json_encode($arr);
		}

	}
?>