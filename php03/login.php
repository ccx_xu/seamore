<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	    <title>西美品牌策划数据管理平台</title>
	    <meta name="description" content="菉竹轩数据管理系统">
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/login.min.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css" rel="stylesheet"/>
	    <!--[if lt IE 9]>
	    <meta http-equiv="refresh" content="0;ie.html" />
	    <![endif]-->
	    <link rel="shortcut icon" href="/icons/mlb_ico.png"/>
	    <style type="text/css">label.error { position:inherit;  }</style>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>		
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/layer.min.js"></script>
		<script src="/js/jquery.blockUI.js" ></script>
		<script src="/js/ccx/ccx-ui.js"></script>
		<script src="/js/ccx/login.js?v=12"></script>
	    <script>
	        if(window.top!==window.self){window.top.location=window.location};
	    </script>
	</head>

	<body class="signin">
	    <div class="signinpanel">
	        <div class="row">
	            <div class="col-sm-7">
	                <div class="signin-info">
	                    <div class="logopanel m-b">
	                        <h1><img alt="[ 西美品牌 ]" width="121px" src="/images/logo.png" ></h1>
	                    </div>
	                    <div class="m-b"></div>
	                    <h4>欢迎使用 <strong>西美品牌策划数据管理平台</strong></h4>
	                    <ul class="m-b">
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 系统管理</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 系统监控</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 系统工具</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 网站管理</li>
	                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 流量监控</li>
	                    </ul>
	                </div>
	            </div>
	            <div class="col-sm-5">
	                <form id="signupForm">
	                    <h4 class="no-margins">登录：</h4>
	                    <p class="m-t-md">所有幸福，都来自平凡的奋斗和坚持</p>
	                    <input type="text"     name="username" class="form-control uname"     placeholder="用户名" value=""    />
	                    <input type="password" name="password" class="form-control pword"     placeholder="密码"   value="" />
						<div class="row m-t">
							<div class="col-xs-6">
							    <input type="text" name="validateCode" class="form-control code" placeholder="验证码" maxlength="5" autocomplete="off">
							</div>
							<div class="col-xs-6">
								<a href="javascript:void(0);" title="点击更换验证码">
									<img src="/view/util/captcha.php?r=<?php echo rand(); ?>" class="imgcode" width="85%"/>
								</a>
							</div>
						</div>
	                    <button class="btn btn-success btn-block" id="btnSubmit" data-loading="正在验证登录，请稍后...">登录</button>
	                </form>
	            </div>
	        </div>
	        <div class="signup-footer">
	            <div class="pull-left">
	                &copy; 2016-2020 All Rights Reserved. @西美品牌策划  &nbsp;&nbsp;   网站备案号：鲁ICP备20018534号 <br>
	            </div>
	        </div>
	    </div>
	</body>
</html>
