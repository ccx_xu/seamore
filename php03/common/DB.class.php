<?php
    //header("content-type:text/html;charset=utf-8");
    class DB{
        public $db_host;//localhost
        public $db_user;//用户名
        public $db_pwd;//密码
        public $db_name;//数据库名
        public $links;//链接名称
        //构造方法的参数和属性名字一致，但是含义不同
        function __construct(){
            //$this -> db_host = "qdm722897608.my3w.com";
            //$this -> db_user = "qdm722897608";
            //$this -> db_pwd = "Shfy@008";
            //$this -> db_name = "qdm722897608_db";
            $this -> db_host = "localhost";
            $this -> db_user = "root";
            $this -> db_pwd = "1234";
            $this -> db_name = "mlb";
            //链接数据库代码
            $this -> links = new mysqli($this->db_host, 
                $this->db_user, $this->db_pwd, $this->db_name);
            // 检测连接
            if ($this -> links->connect_error) {
                die("连接失败: " . $this -> links->connect_error);
            } 
        }

        function query($sql){
            //执行各种sql，inert update delete执行，如果执行select返回结果集
            return $this -> links->query($sql);
        }

        function prepare($prepareSql) {
            return $this->links->prepare($prepareSql);
        }
        
        function numRows($sql){//返回select的记录数
            $result = $this -> query($sql);
            $count = mysql_num_rows($result);
            return $count;
        }

        function getOne($sql){//得到一条记录的一维数组
            $result = $this -> query($sql);
            $arr = mysql_fetch_assoc($result);
            return $arr;
        }

        function getAll($sql){//得到多条记录的二维数组
            $result = $this -> query($sql);
            $rows = array();
            while($rs = $result->fetch_assoc()){
                $rows[] = $rs;
            }
            return $rows;
        }

        function uuid() {     
            static $guid = '';
            $uid = uniqid("", true);
            $data = "";
            $data .= $_SERVER['REQUEST_TIME'];
            $data .= $_SERVER['HTTP_USER_AGENT'];
            $data .= $_SERVER['SERVER_ADDR'];
            $data .= $_SERVER['SERVER_PORT'];
            $data .= $_SERVER['REMOTE_ADDR'];
            $data .= $_SERVER['REMOTE_PORT'];
            $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
            $guid = substr($hash,  0,  8) . 
                substr($hash,  8,  4) .
                substr($hash, 12,  4) .
                substr($hash, 16,  4) .
                substr($hash, 20, 12);
            return $guid;
        }

        function __destruct(){
            $this -> links -> close();
        }

        //返回当前的毫秒时间戳
        function get_msectime() {
            list($msec, $sec) = explode(' ', microtime());
            $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
            return $msectime;
        }

        /** 
          *时间戳 转   日期格式 ： 精确到毫秒，x代表毫秒
          */
        function get_microtime_format($time) {  
            if(strstr($time,'.')){
                sprintf("%01.3f",$time); //小数点。不足三位补0
                list($usec, $sec) = explode(".",$time);
                $sec = str_pad($sec,3,"0",STR_PAD_RIGHT); //不足3位。右边补0
            }else{
                $usec = $time;
                $sec = "000"; 
            }
            $date = date("YmdHisx",$usec);
            return str_replace('x', $sec, $date);
        }

     }
      
     //$db = new DB();
     //$sql = "insert into category(categoryName)values('常熟seo')";
     //$db -> query($sql);
      
     //返回select的记录数
     //$sql = "select * from category";
     //$count = $db -> numRows($sql);
     //echo $count;
      
     //得到一条记录的一维数组
     //$sql = "select * from category where categoryId=1";
     //$arr = $db -> getOne($sql);
     //print_r($arr);
      
     //得到多条记录的二维数组
     //$sql = "select * from mz_user";
     //$rs = $db -> getAll($sql);
     //print_r($rs);
      /*
      $rowsArray = array();
    $sql = "select * from mz_resource where visible='1'";
    $db = new DB();
    $result = $db -> getAll($sql);
    for ($i=0; $i<count($result); $i++) {
        if ($result[$i]['pid']=='0') {
            for ($k=0; $k<count($result); $k++) {
                if ($result[$k]['pid'] == $result[$i]['id']) {
                    $result[$i]['children'][] = $result[$k];
                }
            }
            $rowsArray[] = $result[$i];
        }
    }   
    //print_r($rowsArray);
    for ($i=0; $i<count($rowsArray); $i++) {
        echo $rowsArray[$i]['name'] , "<br/>";
        for ($k=0; $k<count($rowsArray[$i]['children']); $k++) {
            echo "=====", $rowsArray[$i]['children'][$k]['name'], "<br/>";
        }
    }
    */
?>