 <?php
    header("Content-type:text/html;charset=utf-8");

    class Config {
  
        public static function updateconfig($name, $val) {
            $str=file_get_contents(__DIR__."/config.php");
            $str2 = preg_replace("/" . $name . "=(.*);/", $name . "=\"" . $val . "\";",$str);
            file_put_contents(__DIR__."/config.php", $str2);
        }

        public static function getconfig($name) {
            $str=file_get_contents(__DIR__."/config.php");
            $config = preg_match("/" . $name . "=\"(.*)\";/", $str, $res);
            return  $res[1];
        }
    }
?>