<?php 
	class PageInfo {

		/** rows 当前页的数据列表*/
		var $rows=array();
	
		/** total 数据总条数*/
		var $total=0;
	
		/** pageSize 分页大小*/
		var $pageSize = 10;
	
		/** pageNumber 当前页码*/
		var $pageNum = 1;
	
		/** maxPageNum 最大页码*/
		var $maxPageNum = 0;
	
		/** prePage 上一页*/
		var $prePage = 1;
		
		/** nextPage 下一页*/
		var $nextPage = 1;
		
		/** start 开始条*/
		var $start = 0;		
		var $code = 0;
		var $msg = "";

		function __construct() {
			//echo is_numeric($_REQUEST['pageNum'])."<br/>";
			if(isset($_REQUEST['pageNum']) && !empty($_REQUEST['pageNum']) && is_numeric($_REQUEST['pageNum'])) {
				$this->pageNum = $_REQUEST['pageNum'];
			}
			if(isset($_REQUEST['pageSize']) && !empty($_REQUEST['pageSize']) && is_numeric($_REQUEST['pageSize'])) {
				$this->pageSize = $_REQUEST['pageSize'];
			}
			$this->start = ($this->pageNum-1) * $this->pageSize;
			if($this->start < 0) $this->start=0;
			$this->prePage=$this->pageNum-1;
			if($this->prePage<1) $this->prePage=1;
			$this->nextPage=$this->pageNum+1;
			if($this->nextPage>$this->maxPageNum) $this->nextPage=$this->maxPageNum;
		}

		function getStart() {
			echo $this->start;
		}
		function setStart($start) {
			$this->start = $start;
		}
		function getRows() {
			echo $this->rows;
		}
		function setRows($rows) {
			$this->rows = $rows;
		}
		function getTotal() {
			echo $this->total;
		}
		function setTotal($total) {
			$this->total = $total;
			if($this->total%$this->pageSize==0){
				$this->maxPageNum = floor($this->total/$this->pageSize);
			}else {
				$this->maxPageNum = floor($this->total/$this->pageSize) + 1;
			}
			if($this->maxPageNum < 1) $this->maxPageNum=1;
			if($this->pageNum > $this->maxPageNum){
				$this->pageNum = $this->maxPageNum;
			}
		}
		function getPageSize() {
			echo $this->pageSize;
		}
		function setPageSize($pageSize) {
			$this->pageSize = $pageSize;
			$this->start = ($this->pageNum-1) * $this->pageSize;
			if($this->start < 0) $this->start=0;
		}
		function getPageNum() {
			if(is_null($this->pageNum) || $this->pageNum < 1) $this->pageNum=1;
			echo $this->pageNum;
		}
		function setPageNum($pageNum) {
			$this->pageNum = $pageNum;
			$this->start = ($this->pageNum-1) * $this->pageSize;
			if($this->start < 0) $this->start=0;
			$this->prePage=$this->pageNum-1;
			if($this->prePage<1) $this->prePage=1;
			$this->nextPage=$this->pageNum+1;
			if($this->nextPage>$this->maxPageNum) $this->nextPage=$this->maxPageNum;
		}
		function getMaxPageNum() {
			echo $this->maxPageNum;
		}
		function setMaxPageNum($maxPageNum) {
			$this->maxPageNum = $maxPageNum;
		}
		function getPrePage() {
			echo $this->prePage;
		}
		function getNextPage() {
			echo $this->nextPage;
		}
		function getCode() {
			echo $this->code;
		}
		function setCode($code) {
			$this->code = $code;
		}
		function getMsg() {
			echo $this->msg;
		}	
		function setMsg($msg) {
			$this->msg = $msg;
		}
	}
?>