<?php	
	include dirname(__FILE__) . '/../common/Config.class.php';
	$image_path = Config::getconfig("image_path");
	$server_path = Config::getconfig("server_path");
	$phone = Config::getconfig("phone");
	$qq = Config::getconfig("qq");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
		<script type="text/javascript">
		    function avatar() {
		        var url = '${ctx}/system/share';
		        $.modal.open("修改头像", url);
		    }
		    
		    function submitProp() {
		    	if ($.validate.form()) {
		    		var titleText = $("#titleText").val();
		    		var title = $("#title").val();
		    		var shareText = $("#shareText").val();
		    		var tip = $("#tip").val();
		    		var content = $("#content").val();
		    		$.operate.post("${ctx}/prop/save", {"titleText": titleText, "title": title, "shareText": shareText, "tip": tip, "content": content });
		    	}
		    }
		</script>
	</head>

	<body class="gray-bg" style="font: 14px Helvetica Neue, Helvetica, PingFang SC, 微软雅黑, Tahoma, Arial, sans-serif !important;">
    	<input id="userId" name="userId" type="hidden" value="" />
    	<section class="section-content">
    		<div class="row">
		        <div class="col-sm-3 pr5" style="width:800px;position: absolute;">
		            <div class="ibox float-e-margins">
		                <div class="ibox-content">
		                    <div class="tab-content">
		                        <div class="tab-pane active" id="user_info" >
		                            <form class="form-horizontal" id="form-user-edit">
		                                <!--隐藏ID-->
		                                <input name="id" id="id" type="hidden">
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">网站logo：</label>
		                                    <div class="text-center">
						                        <a ><img style="height:50px;" onclick="avatar()" title="点击修改图片" src="<?php echo $server_path ?>/images/logo.png"></a>
						                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">案例图片路径：</label>
		                                    <div class="col-sm-10">
		                                        <input type="text" class="form-control" id="titleText" name="titleText" value="<?php echo $image_path ?>" >
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">网站地址：</label>
		                                    <div class="col-sm-10">
		                                        <input type="text" class="form-control" id="title" name="title" value="<?php echo $server_path ?>" >
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">电话/手机：</label>
		                                    <div class="col-sm-10">
		                                        <input type="text" class="form-control" id="shareText" name="shareText" value="<?php echo $phone ?>" >
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">QQ：</label>
		                                    <div class="col-sm-10">
		                                        <input type="text" class="form-control" id="tip" name="tip" value="<?php echo $qq ?>" >
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label class="col-sm-2 control-label">须知内容：</label>
		                                    <div class="col-sm-10">
		                                        <textarea type="text" class="form-control" id="content" style="min-height:100px;" name="content"  placeholder="请输入须知内容">${content }</textarea>
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <div class="col-sm-offset-2 col-sm-10">
		                                        <button type="button" class="btn btn-sm btn-primary" onclick="submitProp()"><i class="fa fa-check"></i>保 存</button>&nbsp;
		                                        <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
		                                    </div>
		                                </div>
		                            </form>
		                        </div>
			                </div>
			            </div>
			        </div>
          		</div>
    		</div>
		</section>
	</body>
</html>
