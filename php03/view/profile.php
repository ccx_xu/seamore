<?php
	//  防止全局变量造成安全隐患
	$admin = false;
	//  启动会话，这步必不可少
	session_start();
	//  判断是否登陆
	if (isset($_SESSION["admin"]) && $_SESSION["admin"] === true) {
	    
	} else {
	    //  验证失败，将 $_SESSION["admin"] 置为 false
	    header('location:/login.php');  
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
		<script type="text/javascript">
		    /*用户管理-头像*/
		    function avatar() {
		        var url = '/view/avatar.php';
		        $.modal.open("修改头像", url);
		    }
	    
		    /*用户信息-修改*/
		    function check00() {
			    $("#form-user-edit").validate({
					onkeyup: false,
					rules:{
						username:{
							required:true,
				            remote: {
				                url: "/action/user.php?method=checkUsernameUnique",
				                type: "get",
				                dataType: "json",
				                data: {
				        			"username": function() {
				                        return $.common.trim($("#username").val());
				                    }
				                }
				            }
						}
					},
					messages: {
						username: {
			                required: "请输入登录名",
			                remote: "该登录名已存在",
			            }
				    },
				    focusCleanup: true
				});
		    }
		    
		    function check01() {
			    $("#form-user-resetPwd").validate({
					rules:{
						oldPassword:{
							required:true,
							remote: {
			                    url: "/action/user.php?method=checkPassword",
			                    type: "get",
			                    dataType: "json",
			                    data: {
			                        password: function() {
			                            return $("input[name='oldPassword']").val();
			                        }
			                    }
			                }
						},
						newPassword: {
			                required: true,
			                minlength: 5,
			    			maxlength: 20
			            },
			            confirm: {
			                required: true,
			                equalTo: "#newPassword"
			            }
					},
					messages: {
			            oldPassword: {
			                required: "请输入原密码",
			                remote: "原密码错误"
			            },
			            newPassword: {
			                required: "请输入新密码",
			                minlength: "密码不能小于6个字符",
			                maxlength: "密码不能大于20个字符"
			            },
			            confirm: {
			                required: "请再次输入新密码",
			                equalTo: "两次密码输入不一致"
			            }
		
			        },
			        focusCleanup: true
				});
		    }
			
			function submitUserInfo() {
		        if ($.validate.form()) {
		        	$.operate.post("/action/user.php?method=saveprop", $("#form-user-edit").serialize());
		        }
		    }
			
			function submitChangPassword () {
		        if ($.validate.form("form-user-resetPwd")) {
		        	$.operate.saveModal("/action/user.php?method=savepwd", $('#form-user-resetPwd').serialize());
		        }
		    }
		</script>
	</head>

	<body class="gray-bg" style="font: 14px Helvetica Neue, Helvetica, PingFang SC, 微软雅黑, Tahoma, Arial, sans-serif !important;">
    	<input id="userId" name="userId" type="hidden" value="" />
    	<section class="section-content">
		    <div class="row">
		        <div class="col-sm-3 pr5">
		            <div class="ibox float-e-margins">
		                <div class="ibox-title ibox-title-gray dashboard-header gray-bg">
		                    <h5>个人资料</h5>
		                </div>
		                <div class="ibox-content">
		                    <div class="text-center">
		                        <p><img class="img-circle img-lg" src="<?php echo $_SESSION['headImg'] ?>"></p>
		                        <p><a href="javascript:avatar()">修改头像</a></p>
		                    </div>
		                    <ul class="list-group list-group-striped">
		                        <li class="list-group-item"><i class="fa fa-user"></i>
		                            <b class="font-noraml">登录名称：</b>
		                            <p class="pull-right"><?php echo $_SESSION['username'] ?></p>
		                        </li>
		                        <li class="list-group-item"><i class="fa fa-phone"></i>
		                            <b  class="font-noraml">手机号码：</b>
		                            <p class="pull-right"><?php echo $_SESSION['mobile'] ?></p>
		                        </li>
		                        <li class="list-group-item"><i class="fa fa-group"></i>
		                            <b  class="font-noraml">所属单位：</b>
		                            <p class="pull-right" ><?php echo $_SESSION['dept'] ?></p>
		                        </li>
		                        <li class="list-group-item"><i class="fa fa-envelope-o"></i>
		                            <b  class="font-noraml">邮箱地址：</b>
		                            <p class="pull-right" ><?php echo $_SESSION['email'] ?></p>
		                        </li>
		                        <li class="list-group-item"><i class="fa fa-calendar"></i>
		                            <b  class="font-noraml">用户昵称：</b>
		                            <p class="pull-right" ><?php echo $_SESSION['name'] ?></p>
		                        </li>
		                    </ul>
                		</div>
            		</div>
        		</div>
        
		        <div class="col-sm-9 about">
		            <div class="ibox float-e-margins">
		                <div class="ibox-title ibox-title-gray dashboard-header">
		                    <h5>基本资料</h5>
		                </div>
		                <div class="ibox-content">
		                    <div class="nav-tabs-custom">
		                        <ul class="nav nav-tabs">
		                            <li class="active"><a href="#user_info" data-toggle="tab" aria-expanded="true">基本资料</a></li>
		                            <li><a href="#modify_password" data-toggle="tab" aria-expanded="false">修改密码</a></li>
		                        </ul>
		                        <div class="tab-content">
		                            <!--用户信息-->
		                            <div class="tab-pane active" id="user_info">
		                                <form class="form-horizontal" id="form-user-edit">
		                                    <!--隐藏ID-->
		                                    <input name="id" id="id" type="hidden" value="<?php echo $_SESSION['id'] ?>">
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">用户昵称：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" name="name" value="<?php echo $_SESSION['name'] ?>" placeholder="请输入用户昵称">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">用户称谓：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" name="title" value="<?php echo $_SESSION['duty'] ?>" placeholder="请输入用户称谓">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">登录名：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" id="username" onchange="check00()" name="username" value="<?php echo $_SESSION['username'] ?>" placeholder="请输入用户名称">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">手机号码：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" name="mobile" maxlength="11" value="<?php echo $_SESSION['mobile'] ?>" placeholder="请输入手机号码">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">邮箱：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" name="email" value="<?php echo $_SESSION['email'] ?>" placeholder="请输入邮箱">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">所属单位：</label>
		                                        <div class="col-sm-10">
		                                            <input type="text" class="form-control" name="dept" value="<?php echo $_SESSION['dept'] ?>" placeholder="请输入邮箱">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <div class="col-sm-offset-2 col-sm-10">
		                                            <button type="button" class="btn btn-sm btn-primary" onclick="submitUserInfo()"><i class="fa fa-check"></i>保 存</button>&nbsp;
		                                            <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
		                                        </div>
		                                    </div>
		                                </form>
		                            </div>
		                            
		                            <!--修改密码-->
		                            <div class="tab-pane" id="modify_password">
		                                <form class="form-horizontal" id="form-user-resetPwd">
		                                	<input name="id" id="id" type="hidden" value="<?php echo $_SESSION['id'] ?>">
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">旧密码：</label>
		                                        <div class="col-sm-10">
		                                            <input type="password" class="form-control" onkeyup="check01()" name="oldPassword" id="oldPassword" placeholder="请输入旧密码">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">新密码：</label>
		                                        <div class="col-sm-10">
		                                            <input type="password" class="form-control" onkeyup="check01()" name="newPassword" id="newPassword" placeholder="请输入新密码">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <label class="col-sm-2 control-label">确认密码：</label>
		                                        <div class="col-sm-10">
		                                            <input type="password" class="form-control" onkeyup="check01()" name="confirm" placeholder="请确认密码">
		                                        </div>
		                                    </div>
		                                    <div class="form-group">
		                                        <div class="col-sm-offset-2 col-sm-10">
		                                            <button type="button" class="btn btn-sm btn-primary" onclick="submitChangPassword()"><i class="fa fa-check"></i>保 存</button>&nbsp;
		                                            <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
		                                        </div>
		                                    </div>
		                                </form>
		                            </div>
		                        </div>
		                    </div>
		                </div>
            		</div>
        		</div>
        
    		</div>
		</section>
	</body>
</html>
