<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta charset="utf-8">
		<link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- jquery-validate 表单树插件 -->
		<script src="/js/bootstrap-table/bootstrap-treetable.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
	</head>
	<body class="gray-bg">
    <div class="container-div">
		<div class="row">
			<div class="btn-group-sm" id="toolbar" role="group">
	            <a class="btn btn-success" onclick="upload01()" >
		            <i class="fa fa-upload"></i> 上传图片
		        </a>
	            <a class="btn btn-success" onclick="upload02()" >
		            <i class="fa fa-upload"></i> 批量上传
		        </a>
				<a class="btn btn-danger multiple disabled" onclick="$.operate.removeAll()">
		            <i class="fa fa-remove"></i> 删除
		        </a>
	        </div>
	        <div class="col-sm-12 search-collapse">
				<div class="select-list">
					<?php 
						include dirname(__FILE__) . '/../../common/DB.class.php';
						define("IMGPATH", "D:/code/phpdemo/php02");
						$db = new DB();
						$sql = "select name from mz_work where id = ? ";
						$stmt = $db -> prepare($sql);
						$stmt->bind_param("s", $p1);
						// 设置参数并执行
						$p1 = $_REQUEST['workId'];
						// 处理打算执行的SQL命令
						$stmt->execute();
						// 执行SQL语句
						$stmt->store_result();
						// 输出查询的记录个数
					    $stmt->bind_result($r1);
						if ($stmt->fetch())
					    {
					    	//  当验证通过后，启动 Session
					    	// session_start();
					    	//  注册登陆成功的 admin 变量，并赋值 true
					    	$wname = $r1;
					    } 
					    echo "案例标题：".$wname;
					?>
				</div>
			</div>
       		<div class="col-sm-12 select-table table-striped">
	            <table id="bootstrap-table"></table>
	        </div>
	    </div>
	</div>
	
	<script type="text/javascript">
		var addFlag = "";
		var editFlag = "";
		var removeFlag = "";
		var server_url = "http://www.seamore.cn";
		var domain = document.domain;
        if (domain.indexOf('xumzwh')>=0) {
            server_url = "http://www.xumzwh.com";
        }
		var datas = "";
		var prefix = "/view/works";

		$(function() {
			console.log("=="+prefix);
		    var options = {
		    	uniqueId: "id",
		        url: "/action/detail.php?method=list&workId=<?php echo $_REQUEST['workId']?>",
		        removeUrl: "/action/detail.php?method=del&ids={id}",
		        modalName: "用户",
		        columns: [{
		            checkbox: true,
		            width: '5%'
		        }, {
		            title: '文件名',
		            field: 'fileName',
		            width: '15%'
		        }, {
		            title: '缩略图',
		            field: 'filePath',
		            width: '10%',
		            formatter: function(value, row, index) {
		            	return '<a target="_blank" href="'+server_url+value+'" title="点击查看大图"><img height="50px" src="'+server_url+value+'"></a>';
                    }
		        }, {
		            field: 'orderNum',
		            title: '显示次序',
		            width: '10%',
		            align: 'center'
		        }, {
		            field: 'state',
		            title: '是否可用',
		            width: '10%',
		            align: "center",
		            formatter: function(value, row, index) {
		            	if (row.state=='1') {
		            		return '<i class=\"fa fa-toggle-on text-info fa-2x\" onclick="disable(\'' + row.id + '\')"></i> ';
			    		} else {
			    			return '<i class=\"fa fa-toggle-off text-info fa-2x\" onclick="enable(\'' + row.id + '\')"></i> ';
			    		}
                    }
		        }, {
		            field: 'mainFlg',
		            title: '设置主图',
		            width: '10%',
		            align: "center",
		            formatter: function(value, row, index) {
		            	if (row.mainFlg=='1') {
		            		return '<i class=\"fa fa-toggle-on text-info fa-2x\" onclick="dismain(\'' + row.id + '\', \'' + row.workId + '\')"></i> ';
			    		} else {
			    			return '<i class=\"fa fa-toggle-off text-info fa-2x\" onclick="main(\'' + row.id + '\', \'' + row.workId + '\')"></i> ';
			    		}
                    }
		        }, {
		            title: '操作',
		            width: '20%',
		            align: "center",
		            formatter: function(value, row, index) {
		                var actions = [];
		                actions.push('<a class="btn btn-danger btn-xs ' + removeFlag + '" href="javascript:void(0)" onclick="$.operate.remove(\'' + row.id + '\')"><i class="fa fa-trash"></i>删除</a>');
		                return actions.join('');
		            }
		        }]
		    };
		    $.table.init(options);
		});
		
		function upload01() {
			var url = prefix+"/upload01.php?workId=<?php echo $_REQUEST['workId']?>";
			$.modal.open("添加案例", url, 900, 600);
		}
		
		function upload02(id) {
			var url = prefix+"/upload02.php?workId=<?php echo $_REQUEST['workId']?>";
			$.modal.open("修改案例", url, 900, 600);
		}

		function disable(id) {
			$.modal.confirm("确认要停用当前图片吗？", function() {
				$.operate.post("/action/detail.php?method=save", { "id": id, "state": 0 });
		    })
		}

		function enable(id) {
			$.modal.confirm("确认要使用当前图片吗？", function() {
				$.operate.post("/action/detail.php?method=save", { "id": id, "state": 1 });
		    })
		}

		/* 用户管理-停用 */
		function dismain(id, workId) {
			$.modal.confirm("确认要取消主图吗？", function() {
				$.operate.post("/action/detail.php?method=save", { "id": id, "mainFlg": 0, "workId": workId });
		    })
		}

		/* 用户管理启用 */
		function main(id, workId) {
			$.modal.confirm("确认要要将当前图片设置成主图吗？", function() {
				$.operate.post("/action/detail.php?method=save", { "id": id, "mainFlg": 1, "workId": workId });
		    })
		}
	</script>
</body>
</html>