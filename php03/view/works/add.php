<?php 
	include dirname(__FILE__) . '/../../common/DB.class.php';
	$cates = array();
	$db = new DB();
	$sql = "select id_key, name from mz_category where state='1'";
	$stmt = $db -> prepare($sql);
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($key, $name);
	while ($stmt->fetch())
    {
    	$tmpArr = array();
    	$tmpArr['key'] = $key;
    	$tmpArr['text'] = $name;
    	$cates[] = $tmpArr;
    } 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <link href="/css/date/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	    
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.2.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
		
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.config.js?v=1.2"></script>
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.all.js"> </script>
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.all.min.js?v=1.1"> </script>
	    <script type="text/javascript" charset="utf-8" src="/js/date/bootstrap-datetimepicker.min.js"></script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/zh-cn.js"></script>
	
	    <style type="text/css">
	        div{
	            width:100%;
	        }
	        .article_left {
	        	position: absolute;
	        	width:100px;
	        }
	        .article_right {
	        	position: inherit;
	        	margin-left: 100px;
	        }
	        .laydate-theme-molv {
			    border: none;
			    margin-left:100px;
			    width:274px;
			}
			.laydate-footer-btns {
			    position: absolute;
			    right: 10px;
			    top: 10px;
			    left: 10px;
			}
	    </style>
	</head>
	<body>
		<div class="tab-content">
			<div class="tab-pane active" id="user_info">
				<form class="form-horizontal" id="form-article-add">
		            <!--隐藏ID-->
		            <input name="id" id="id" type="hidden">
		            <input id="imageUrl_" name="imageUrl_" type="hidden" value="" />
		            <div class="form-group" style="margin-top: 15px;">
		                <label class="control-label article_left">案例标题：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="name" name="name" value="" placeholder="请输入案例标题">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">中文名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="nameCn" name="nameCn" value="" placeholder="请输入中文名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">英文名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="nameEn" name="nameEn" value="" placeholder="请输入英文名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">案例封面：</label>
		                <div class="col-sm-11 article_right" style="height:150px;">
		                    <img id="forntImage" style="position:absolute;width:220px;heigth:150px;" src="http://www.seamore.cn/works/20200411/86312a8cfe379631696e017083e8e1ce.jpg">
							<input style="position:inherit;width:150px;margin-left:240px;margin-top:20px;" name="imgFile" id="imgFile" type="file" accept="image/*"/>
							<input type="button" style="position:inherit;width:50px;margin-left:240px;margin-top:20px;" value="上传" onclick="upload()"/><br/>
							<label style="position:inherit;width:600px;margin-left:240px;margin-top:20px;">说明：案例封面显示在网站首页, 为显示美观，请保存图片尺寸一致, 参考尺寸：368x243</label>
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">客户名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="cname" name="cname" value="" placeholder="请输入客户名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">所属行业：</label>
		                <div class="col-sm-11 article_right">
		                    <select type="text" class="form-control" name="cateId" value="">
		                    	<?php  for ($i=0; $i<count($cates); $i++) {	?>
		                    		<option value="<?php echo $cates[$i]['key'] ?>"><?php echo $cates[$i]['text'] ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">服务时间：</label>
		                <div class="col-sm-3 article_right">
		                    <input type="text" class="form-control time-input" id="sdate" data-format="yyyy-MM-dd" name="sdate" value="" placeholder="2000-01-01">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">服务项目：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" name="sname" value="" placeholder="请输入关键字">
		                </div>
		            </div>
		            <div class="form-group">
						<label class="control-label article_left">显示排序：</label>
						<div class="col-sm-11 article_right">
							<input class="form-control" type="text" name="orderNum" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label article_left">是否显示：</label>
						<div class="col-sm-11 article_right">
							<label class="radio-box" >
								<input type="radio" name="state" value="1" checked/>
								显示
							</label>
							<label class="radio-box" >
								<input type="radio" name="state" value="0"/>
								隐藏
							</label>
						</div>
					</div>
		            <div class="form-group">
		                <label class="control-label article_left">案例介绍：</label>
		                <div class="col-sm-11 article_right">
		                    <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
		                </div>
		            </div>
		            <div class="form-group">
	                    <div class="col-sm-offset-2 col-sm-10">
	                        <button type="button" class="btn btn-sm btn-primary" onclick="saveWorks()"><i class="fa fa-check"></i>保 存</button>&nbsp;
	                        <button type="button" class="btn btn-sm btn-danger" onclick="closeItem()"><i class="fa fa-reply-all"></i>关 闭 </button>
	                    </div>
	                </div>
				</form>
			</div>
		</div>
		<script type="text/javascript">
		    //实例化编辑器
		    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
		    var ue = UE.getEditor('editor', {
		    	toolbars: [
				    ['fullscreen', 'source', 'undo', 'redo','bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', 'fontfamily', 'fontsize']
				]
		    });
		    var val_type = 1;
		    function saveWorks() {
		    	var myForm = document.getElementById("form-article-add");
		    	var formData = new FormData(myForm);
		    	formData.append("htmlContent", UE.getEditor('editor').getContent());
		    	console.log(formData.get('htmlContent'));
		    	$.ajax({
		    	    url: '/action/works.php?method=add',
		    	    type: 'POST',
		    	    data: formData,                    // 上传formdata封装的数据
		    	    dataType: 'JSON',
		    	    cache: false,                      // 不缓存
		    	    processData: false,                // jQuery不要去处理发送的数据
		    	    contentType: false,                // jQuery不要去设置Content-Type请求头
		    	    success:function (result) {           //成功回调
	    	    		parent.$.modal.msgSuccess(result.msg);
		    	    }
		    	});
		    }
		    
		    function doSubmit(index, layero){
				var body = layer.getChildFrame('body', index);
				console.log("val_type="+val_type);
				if (val_type==1) {
	   				$("#mainColKey").val(body.find('#treeId').val());
	   				$("#mainColName").val(body.find('#treeName').val());
				} else if (val_type==2) {
					$("#slaveColKey").val(body.find('#treeId').val());
	   				$("#slaveColName").val(body.find('#treeName').val());
				}
	   			layer.close(index);
			}
		    
		    function upload() {
            	var formData = new FormData();
            	formData.append("imgFile",$('#imgFile')[0].files[0]);
            	
                $.ajax({
                    type : "POST",
                    url : "/action/upload_file.php",
                    data : formData,
                    async : false,
                    processData : false, // 使数据不做处理
                    contentType : false, // 不要设置Content-Type请求头
                    error : function(request) {
                        $.modal.alertError("系统错误");
                    },
                    success : function(data) {
                    	var res = eval('(' + data + ')');
                    	var domain = document.domain;
                    	if (domain.indexOf('seamore')>=0) {
                            $("#forntImage").attr("src", "http://www.seamore.cn" + res.src);
                        } else {
                            $("#forntImage").attr("src", "http://www.xumzwh.com" + res.src);
                        }
                        $("#imageUrl_").val(res.src);
                    }
                });
            }
		</script>
	</body>
</html>