<?php
	include dirname(__FILE__) . '/../../common/DB.class.php';
	$h_domain = $_SERVER['HTTP_HOST'];
	$serverhost = 'http://www.seamore.cn';
	if (strpos($h_domain, 'xumzwh') !== false) {
	    define("IMGPATH", "D:/code/phpdemo/php02");
	    $serverhost = 'http://www.xumzwh.com';
	} else {
	    define("IMGPATH", "/data/home/qyu7548620001/htdocs");
	}
	$id = $_REQUEST['id'];

	$db = new DB();

	$sql = "select id, name, name_cn, name_en, cate_id, image_url, order_num, c_name, s_date, s_name, descc, state from mz_work where id = ? ";
	$stmt = $db -> prepare($sql);
	$stmt->bind_param("s", $p1);
	 
	// 设置参数并执行
	$p1 = $id;
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($r1, $r2, $r3, $r4, $r5, $r6, $r7, $r8, $r9, $r10, $r11, $r12);
	if ($stmt->fetch())
    {
    	//  当验证通过后，启动 Session
    	// session_start();
    	//  注册登陆成功的 admin 变量，并赋值 true
    	$wname = $r2;
    	$name_cn = $r3;
    	$name_en = $r4;
    	$cate_id = $r5;
    	$image_url = $r6;
    	$order_num = $r7;
    	$c_name = $r8;
    	$s_date = $r9;
    	$s_name = $r10;
    	$descc = $r11;
    	$state = $r12;
    }

	$cates = array();
	$db = new DB();
	$sql = "select id_key, name from mz_category where state='1'";
	$stmt = $db -> prepare($sql);
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($key, $name);
	while ($stmt->fetch())
    {
    	$tmpArr = array();
    	$tmpArr['key'] = $key;
    	$tmpArr['text'] = $name;
    	$cates[] = $tmpArr;
    } 

    $file = fopen(IMGPATH.$descc, "r") or exit("无法打开文件!");
	// 读取文件每一行，直到文件结尾
	$htmlContent = "";
	while(!feof($file))
	{
	    $htmlContent .= fgets($file). "<br>";
	}
	fclose($file);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <link href="/css/date/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
	    
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.2.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
		
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.config.js?v=1.2"></script>
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.all.js"> </script>
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/ueditor.all.min.js?v=1.1"> </script>
	    <script type="text/javascript" charset="utf-8" src="/js/date/bootstrap-datetimepicker.min.js"></script>
	    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
	    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
	    <script type="text/javascript" charset="utf-8" src="/js/ueditor/zh-cn.js"></script>
	
	    <style type="text/css">
	        div{
	            width:100%;
	        }
	        .article_left {
	        	position: absolute;
	        	width:100px;
	        }
	        .article_right {
	        	position: inherit;
	        	margin-left: 100px;
	        }
	        .laydate-theme-molv {
			    border: none;
			    margin-left:100px;
			    width:274px;
			}
			.laydate-footer-btns {
			    position: absolute;
			    right: 10px;
			    top: 10px;
			    left: 10px;
			}
	    </style>
	</head>
	<body>
		<div class="tab-content">
			<div class="tab-pane active" id="user_info">
				<form class="form-horizontal" id="form-article-add">
		            <!--隐藏ID-->
		            <input name="id" id="id" type="hidden" value="<?php echo $id ?>">
		            <input id="imageUrl_" name="imageUrl_" type="hidden" value="<?php echo $image_url ?>" />
		            <div class="form-group" style="margin-top: 15px;">
		                <label class="control-label article_left">案例标题：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $wname ?>" placeholder="请输入案例标题">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">中文名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="nameCn" name="nameCn" value="<?php echo $name_cn ?>" placeholder="请输入中文名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">英文名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="nameEn" name="nameEn" value="<?php echo $name_en ?>" placeholder="请输入英文名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">案例封面：</label>
		                <div class="col-sm-11 article_right" style="height:150px;">
		                    <img id="forntImage" style="position:absolute;width:220px;heigth:150px;" src="<?php echo $serverhost ?>/<?php echo $image_url ?>">
							<input style="position:inherit;width:150px;margin-left:240px;margin-top:20px;" name="imgFile" id="imgFile" type="file" accept="image/*"/>
							<input type="button" style="position:inherit;width:50px;margin-left:240px;margin-top:20px;" value="上传" onclick="upload()"/>
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">客户名称：</label>
		                <div class="col-sm-11 article_right">
		                    <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $c_name ?>" placeholder="请输入客户名称">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">所属行业：</label>
		                <div class="col-sm-11 article_right">
		                    <select type="text" class="form-control" name="cateId" value="<?php echo $cate_id ?>">
		                    	<?php  for ($i=0; $i<count($cates); $i++) {	?>
		                    		<option value="<?php echo $cates[$i]['key'] ?>"><?php echo $cates[$i]['text'] ?></option>
		                    	<?php } ?>
		                    </select>
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">服务时间：</label>
		                <div class="col-sm-3 article_right">
		                    <input class="form-control time-input" data-format="yyyy-MM-dd" name="sdate" value="<?php echo $s_date ?>" placeholder="2000-01-01">
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="control-label article_left">服务项目：</label>
		                <div class="col-sm-11 article_right">
		                    <input class="form-control" name="sname" value="<?php echo $s_name ?>" placeholder="请输入关键字">
		                </div>
		            </div>
		            <div class="form-group">
						<label class="control-label article_left">显示排序：</label>
						<div class="col-sm-11 article_right">
							<input class="form-control" name="orderNum" value="<?php echo $order_num ?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label article_left">是否显示：</label>
						<div class="col-sm-11 article_right">
							<label class="radio-box" >
								<input type="radio" name="state" value="1" <?php echo $state==='1'?'checked':'' ?> />
								显示
							</label>
							<label class="radio-box" >
								<input type="radio" name="state" value="0" <?php echo $state==='0'?'checked':'' ?>/>
								隐藏
							</label>
						</div>
					</div>
		            <div class="form-group">
		                <label class="control-label article_left">案例介绍：</label>
		                <div class="col-sm-11 article_right">
		                    <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
		                </div>
		            </div>
		            
				</form>
			</div>
		</div>
		<script type="text/javascript">
		    //实例化编辑器
		    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
		    var ue = UE.getEditor('editor');
		    var val_type = 1;
		    ue.ready(function() {
				//设置编辑器的内容"
				ue.setContent('<?php echo $htmlContent ?>');  //date为动态传入的数据
			});
		    
		    /*
		    $(function(){
		    	console.log("${uedtor01}");
		    	UE.getEditor('editor').setContent("${uedtor01}");
		    	//UE.getEditor('editor').execCommand("insertHtml", "${uedtor01}");
		    });
		    */
		
		    function isFocus(e){
		        alert(UE.getEditor('editor').isFocus());
		        UE.dom.domUtils.preventDefault(e)
		    }
		    function setblur(e){
		        UE.getEditor('editor').blur();
		        UE.dom.domUtils.preventDefault(e)
		    }
		    function insertHtml() {
		        var value = prompt('插入html代码', '');
		        UE.getEditor('editor').execCommand('insertHtml', value)
		    }
		    function createEditor() {
		        enableBtn();
		        UE.getEditor('editor');
		    }
		    function getAllHtml() {
		        alert(ue.getAllHtml());
		    }
		    function getContent() {
		        var arr = [];
		        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
		        arr.push("内容为：");
		        arr.push(UE.getEditor('editor').getContent());
		        alert(arr.join("\n"));
		    }
		    function getPlainTxt() {
		        var arr = [];
		        arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
		        arr.push("内容为：");
		        arr.push(UE.getEditor('editor').getPlainTxt());
		        alert(arr.join('\n'))
		    }
		    function setContent(isAppendTo) {
		        var arr = [];
		        arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
		        ue.setContent('欢迎使用ueditor', isAppendTo);
		        //alert(arr.join("\n"));
		    }
		    function setDisabled() {
		        UE.getEditor('editor').setDisabled('fullscreen');
		        disableBtn("enable");
		    }
		
		    function setEnabled() {
		        UE.getEditor('editor').setEnabled();
		        enableBtn();
		    }
		
		    function getText() {
		        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
		        var range = UE.getEditor('editor').selection.getRange();
		        range.select();
		        var txt = UE.getEditor('editor').selection.getText();
		        alert(txt)
		    }
		
		    function getContentTxt() {
		        var arr = [];
		        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
		        arr.push("编辑器的纯文本内容为：");
		        arr.push(UE.getEditor('editor').getContentTxt());
		        alert(arr.join("\n"));
		    }
		    function hasContent() {
		        var arr = [];
		        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
		        arr.push("判断结果为：");
		        arr.push(UE.getEditor('editor').hasContents());
		        alert(arr.join("\n"));
		    }
		    function setFocus() {
		        UE.getEditor('editor').focus();
		    }
		    function deleteEditor() {
		        disableBtn();
		        UE.getEditor('editor').destroy();
		    }
		    function disableBtn(str) {
		        var div = document.getElementById('btns');
		        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
		        for (var i = 0, btn; btn = btns[i++];) {
		            if (btn.id == str) {
		                UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
		            } else {
		                btn.setAttribute("disabled", "true");
		            }
		        }
		    }
		    function enableBtn() {
		        var div = document.getElementById('btns');
		        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
		        for (var i = 0, btn; btn = btns[i++];) {
		            UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
		        }
		    }
		
		    function getLocalData () {
		        alert(UE.getEditor('editor').execCommand( "getlocaldata" ));
		    }
		
		    function clearLocalData () {
		        UE.getEditor('editor').execCommand( "clearlocaldata" );
		        alert("已清空草稿箱")
		    }
		    
		    /*菜单管理-新增-选择菜单树*/
	        function selectColTree(val) {
	        	var treeId = $("#mainColKey").val();
	        	val_type = val;
	        	if (val_type == 2) {
	        		treeId = $("#slaveColKey").val();
	        	}
	        	var menuId = treeId > 0 ? treeId : 1;
	        	var url = "${ctx}/article/column/" + menuId;
				var options = {
					title: '栏目选择',
					width: "380",
					height: "400",
					url: url,
					callBack: doSubmit
				};
				$.modal.openOptions(options);
			}
		    
		    function submitHandler() {
		    	var myForm = document.getElementById("form-article-add");
		    	var formData = new FormData(myForm);
		    	formData.append("digest", UE.getEditor('editor').getContentTxt());
		    	formData.append("htmlContent", UE.getEditor('editor').getContent());
		    	//console.log(formData.get('htmlContent'));
		    	$.ajax({
		    	    url: '/action/works.php?method=edit',
		    	    type: 'POST',
		    	    data: formData,                    // 上传formdata封装的数据
		    	    dataType: 'JSON',
		    	    cache: false,                      // 不缓存
		    	    processData: false,                // jQuery不要去处理发送的数据
		    	    contentType: false,                // jQuery不要去设置Content-Type请求头
		    	    success:function (result) {           //成功回调
	    	    		$.modal.msgSuccess(result.msg);
		    	    }
		    	});
		    }
		    
		    function doSubmit(index, layero){
				var body = layer.getChildFrame('body', index);
				if (val_type == 1) {
	   				$("#mainColKey").val(body.find('#treeId').val());
	   				$("#mainColName").val(body.find('#treeName').val());
				} else if (val_type == 2) {
					$("#slaveColKey").val(body.find('#treeId').val());
	   				$("#slaveColName").val(body.find('#treeName').val());
				}
	   			layer.close(index);
			}
		   	
            function upload() {
            	var formData = new FormData();
            	formData.append("imgFile",$('#imgFile')[0].files[0]);
                $.ajax({
                    type : "POST",
                    url : "/action/upload_file.php",
                    data : formData,
                    async : false,
                    processData : false, // 使数据不做处理
                    contentType : false, // 不要设置Content-Type请求头
                    error : function(request) {
                        $.modal.alertError("系统错误");
                    },
                    success : function(data) {
                    	console.log(data.src);
                    	var res = eval('(' + data + ')');
                        var domain = document.domain;
                        if (domain.indexOf('xumzwh')>=0) {
                            $("#forntImage").attr("src", "http://www.xumzwh.com/" + res.src);
                        } else {
                            $("#forntImage").attr("src", "http://www.seamore.cn/" + res.src);
                        }
                        $("#imageUrl_").val(res.src);
                    }
                });
            }
		    
		</script>
	</body>
</html>