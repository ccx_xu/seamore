<?php 
	include dirname(__FILE__) . '/../../common/DB.class.php';
	$id = $_REQUEST['id'];

	$db = new DB();

	$sql = "select id, name, username, email, tele, fax, mobile, status from mz_user where id = ? ";
	$stmt = $db -> prepare($sql);
	$stmt->bind_param("s", $p1);
	 
	// 设置参数并执行
	$p1 = $id;
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($r1, $r2, $r3, $r4, $r5, $r6, $r7, $r8);
	if ($stmt->fetch())
    {
    	$name = $r2;
    	$username = $r3;
    	$email = $r4;
    	$tele = $r5;
    	$fax = $r6;
    	$mobile = $r7;
    	$status = $r8;
    } 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
	</head>
	<body class="white-bg">
		<div class="wrapper wrapper-content animated fadeInRight ibox-content">
			<form class="form-horizontal m" id="form-user-edit">
				<input id="id" name="id" type="hidden" value="<?php echo $id ?>"/>
				<div class="form-group">
					<label class="col-sm-3 control-label is-required">用户名称：</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="name" id="name" value="<?php echo isset($name)?$name:'' ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label is-required">用户名：</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="username" id="username" value="<?php echo $username ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">邮箱：</label>
					<div class="col-sm-8">
						<input id="email" name="email" class="form-control" value="<?php echo $email ?>" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">电话：</label>
					<div class="col-sm-8">
						<input id="tele" name="tele" class="form-control" value="<?php echo $tele ?>" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">传真：</label>
					<div class="col-sm-8">
						<input id="fax" name="fax" class="form-control" value="<?php echo $fax ?>" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">手机：</label>
					<div class="col-sm-8">
						<input id="mobile" name="mobile" class="form-control" value="<?php echo $mobile ?>" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">是否禁用：</label>
					<div class="col-sm-8">
					    <label class="radio-box" >
							<input type="radio" name="status" value="1" <?php echo $status=='1'?'checked':'' ?> />
							是
						</label>
						<label class="radio-box" >
							<input type="radio" name="status" value="0" <?php echo $status=='0'?'checked':'' ?> />
							否
						</label>
					</div>
				</div>
			</form>
		</div>
	 	<script>
	        	        
	        function submitHandler() {
		        if ($.validate.form()) {
		            $.operate.save("/action/user.php?method=edit", $('#form-user-edit').serialize());
		        }
		    }
	
	    </script>
	</body>
</html>
