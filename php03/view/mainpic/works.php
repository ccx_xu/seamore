<?php
	$id = $_REQUEST['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <meta charset="utf-8">
		<link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>

		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- jquery-validate 表单树插件 -->
		<script src="/js/bootstrap-table/bootstrap-treetable.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
	</head>
	<body class="gray-bg">
        <div class="container-div">
            <div class="row">
                <div class="col-sm-12 search-collapse">
                    <form id="menu-form">
                        <div class="select-list">
                            <ul>
                                <li>
                                    案例标题：<input type="text" name="name"/>
                                </li>
                                <li>
                                    <a class="btn btn-primary btn-rounded btn-sm" onclick="$.table.search()"><i class="fa fa-search"></i>&nbsp;搜索</a>
                                    <a class="btn btn-warning btn-rounded btn-sm" onclick="$.form.reset()"><i class="fa fa-refresh"></i>&nbsp;重置</a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 select-table table-striped">
                    <table id="bootstrap-table"></table>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var addFlag = "";
            var editFlag = "";
            var removeFlag = "";
            var datas = "";
            var prefix = "/view/works";

            $(function() {
                console.log("=="+prefix);
                var options = {
                    uniqueId: "id",
                    url: "/action/works.php?method=list&state=1",
                    createUrl: prefix + "/add/{id}",
                    updateUrl: prefix + "/edit/{id}",
                    removeUrl: "/action/works.php?method=del&ids={id}",
                    modalName: "用户",
                    showSearch: false,
                    showRefresh: false,
                    showColumns: false,
                    showToggle: false,
                    columns: [{
                        title: '案例标题',
                        field: 'name',
                        width: '35%'
                    }, {
                        field: 'nameCn',
                        title: '中文名称',
                        width: '30%'
                    }, {
                        field: 'nameEn',
                        title: '英文名称',
                        width: '25%',
                        align: 'center'
                    }, {
                        title: '操作',
                        width: '10%',
                        align: "center",
                        formatter: function(value, row, index) {
                            var actions = [];
                            actions.push('<a class="btn btn-success btn-xs ' + editFlag + '" href="javascript:void(0)" onclick="doRelation(\'' + row.id + '\')"><i class="fa fa-edit"></i> 关联</a> ');
                            return actions.join('');
                        }
                    }]
                };
                $.table.init(options);
            });

            function doRelation(workId) {
                var id = '<?php echo $id ?>';
                var formData = new FormData();
                formData.append("id", id);
                formData.append("workId", workId);
                $.ajax({
                    url: '/action/mainpic.php?method=save',
                    type: 'POST',
                    data: formData,                    // 上传formdata封装的数据
                    dataType: 'JSON',
                    cache: false,                      // 不缓存
                    processData: false,                // jQuery不要去处理发送的数据
                    contentType: false,                // jQuery不要去设置Content-Type请求头
                    success:function (result) {           //成功回调
                        $.modal.msgSuccess(result.msg);
                        submitHandler();
                    }
                });
            }

            function submitHandler() {
                parent.$.table.search();
                $.modal.close();
            }
        </script>
    </body>
</html>
