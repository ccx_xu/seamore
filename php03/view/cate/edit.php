<?php 
	include dirname(__FILE__) . '/../../common/DB.class.php';
	$id = $_REQUEST['id'];

	$db = new DB();

	$sql = "select id, id_key, name, cate_desc, order_num, state from mz_category where id = ? ";
	$stmt = $db -> prepare($sql);
	$stmt->bind_param("s", $p1);
	 
	// 设置参数并执行
	$p1 = $id;
	// 处理打算执行的SQL命令
	$stmt->execute();
	// 执行SQL语句
	$stmt->store_result();
	// 输出查询的记录个数
    $stmt->bind_result($r1, $r2, $r3, $r4, $r5, $r6);
	if ($stmt->fetch())
    {
    	//  当验证通过后，启动 Session
    	// session_start();
    	//  注册登陆成功的 admin 变量，并赋值 true
    	$key = $r2;
    	$name = $r3;
    	$descc = $r4;
    	$orderNum = $r5;
    	$state = $r6;
    	$arr['code'] = 0;
    } else {
    	$arr['code'] = 1;
   		$arr['msg'] = "用户名或密码错误！";
   	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
	</head>
	<body class="white-bg">
		<div class="wrapper wrapper-content animated fadeInRight ibox-content">
			<form class="form-horizontal m" id="form-cate-edit">
				<input id="id" name="id" type="hidden" value="<?php echo $id ?>"/>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别标识：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="key" id="key" value="<?php echo $key ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别名称：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="name" id="name" value="<?php echo $name ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别描述：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="descc" id="descc" value="<?php echo $descc ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">显示排序：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="orderNum" value="<?php echo $orderNum ?>" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">是否显示：</label>
					<div class="col-sm-10">
						<label class="radio-box" >
							<input type="radio" name="state" value="1" <?php echo $state==='1'?'checked':'' ?> />
							显示
						</label>
						<label class="radio-box" >
							<input type="radio" name="state" value="0" <?php echo $state==='0'?'checked':'' ?> />
							隐藏
						</label>
					</div>
				</div>
			</form>
		</div>
	 	<script>
	        var prefix = "${ctx}/column";
	        
	        $("#form-menu-add").validate({
	        	onkeyup: false,
	        	rules:{
	        		menuType:{
	        			required:true,
	        		},
	        		menuName:{
	        			remote: {
	                        url: prefix + "/checkMenuNameUnique",
	                        type: "post",
	                        dataType: "json",
	                        data: {
	                        	"parentId": function() {
			                		return $("input[name='parentId']").val();
			                    },
	                        	"menuName" : function() {
	                                return $.common.trim($("#menuName").val());
	                            }
	                        },
	                        dataFilter: function(data, type) {
	                        	return $.validate.unique(data);
	                        }
	                    }
	        		},
	        		orderNum:{
	        			digits:true
	        		},
	        	},
	        	messages: {
	                "menuName": {
	                    remote: "菜单已经存在"
	                }
	            },
	            focusCleanup: true
	        });
	        
	        function submitHandler() {
		        if ($.validate.form()) {
		            $.operate.save("/action/category.php?method=edit", $('#form-cate-edit').serialize());
		        }
		    }
	
	        $(function() {
	        	$("input[name='icon']").focus(function() {
	                $(".icon-drop").show();
	            });
	        	$("#form-menu-add").click(function(event) {
	        	    var obj = event.srcElement || event.target;
	        	    if (!$(obj).is("input[name='icon']")) {
	        	    	$(".icon-drop").hide();
	        	    }
	        	});
	        	$(".icon-drop").find(".ico-list i").on("click", function() {
	        		$('#icon').val($(this).attr('class'));
	            });
	        	$('input').on('ifChecked', function(event){  
	        		var menuType = $(event.target).val();
	        		if (menuType == "M") {
	                    $("#url").parents(".form-group").hide();
	                    $("#perms").parents(".form-group").hide();
	                    $("#icon").parents(".form-group").show();
	                    $("#target").parents(".form-group").hide();
	                    $("input[name='visible']").parents(".form-group").show();
	                } else if (menuType == "C") {
	                	$("#url").parents(".form-group").show();
	                    $("#perms").parents(".form-group").show();
	                    $("#icon").parents(".form-group").show();
	                    $("#target").parents(".form-group").show();
	                    $("input[name='visible']").parents(".form-group").show();
	                } else if (menuType == "F") {
	                	$("#url").parents(".form-group").hide();
	                    $("#perms").parents(".form-group").show();
	                    $("#icon").parents(".form-group").hide();
	                    $("#target").parents(".form-group").hide();
	                    $("input[name='visible']").parents(".form-group").hide();
	                }
	        	});  
	        });
	
	        /*菜单管理-新增-选择菜单树*/
	        function selectMenuTree() {
	        	var treeId = $("#treeId").val();
	        	var menuId = treeId > 0 ? treeId : 1;
	        	var url = prefix + "/selectMenuTree/" + menuId;
				var options = {
					title: '菜单选择',
					width: "380",
					url: url,
					callBack: doSubmit
				};
				$.modal.openOptions(options);
			}
			
			function doSubmit(index, layero){
				var body = layer.getChildFrame('body', index);
	   			$("#treeId").val(body.find('#treeId').val());
	   			$("#treeName").val(body.find('#treeName').val());
	   			layer.close(index);
			}
			
			function upload() {
            	var formData = new FormData();
            	formData.append("imgFile",$('#imgFile')[0].files[0]);
            	
                $.ajax({
                    type : "POST",
                    url : "<%=request.getContextPath()%>/column/image/upload",
                    data : formData,
                    async : false,
                    processData : false, // 使数据不做处理
                    contentType : false, // 不要设置Content-Type请求头
                    error : function(request) {
                        $.modal.alertError("系统错误");
                    },
                    success : function(data) {
                        $("#forntImage").attr("src", data.src);
                        $("#imageUrl_").val("1");
                        $("#ext").val(data.ext);
                    }
                });
            }
	    </script>
	</body>
</html>
