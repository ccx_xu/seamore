<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link href="/icons/favicon.ico" rel="stylesheet"/>
	    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	    <link href="/css/jquery.contextMenu.min.css" rel="stylesheet"/>
	    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
	    <link href="/css/animate.css" rel="stylesheet"/>
	    <link href="/css/style.css" rel="stylesheet"/>
	    <link href="/css/skins.css" rel="stylesheet"/>
	    <link href="/css/ccx/ccx-ui.css?v=4.1.0" rel="stylesheet"/>
	    <!-- 全局js -->
		<script src="/js/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="/js/jquery.contextMenu.min.js"></script>
		<script src="/js/jquery.blockUI.js"></script>
		<script src="/js/layer.min.js"></script>
		
		<!-- bootstrap-table 表格插件 -->
		<script src="/js/bootstrap-table/bootstrap-table.min.js?v=20191219"></script>
		<script src="/js/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
		<script src="/js/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js"></script>
		<script src="/js/bootstrap-table/extensions/toolbar/bootstrap-table-toolbar.min.js"></script>
		<script src="/js/bootstrap-table/extensions/columns/bootstrap-table-fixed-columns.js"></script>
		<!-- jquery-validate 表单验证插件 -->
		<script src="/js/validate/jquery.validate.min.js"></script>
		<script src="/js/validate/messages_zh.min.js"></script>
		<script src="/js/validate/jquery.validate.extend.js"></script>
		<!-- 遮罩层 -->
	    <script src="/js/iCheck/icheck.min.js"></script>
		<script src="/js/layui/layui.js"></script>
		
		<script src="/js/ccx/ccx-ui.js?v=4.1.0"></script>
		<script src="/js/ccx/common.js?v=4.1.0"></script>
		<script src="/js/ccx/index.js"></script>
		<script src="/js/jquery.fullscreen.js"></script>
	</head>
	<body class="white-bg">
		<div class="wrapper wrapper-content animated fadeInRight ibox-content">
			<form class="form-horizontal m" id="form-cate-add">
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别标识：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="key" id="key" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别名称：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="name" id="name" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">行业类别描述：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="descc" id="descc" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label is-required" style="padding-left:0px;">显示排序：</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="orderNum" value="" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" style="padding-left:0px;">是否显示：</label>
					<div class="col-sm-10">
						<label class="radio-box" >
							<input type="radio" name="state" value="1" checked/>
							显示
						</label>
						<label class="radio-box" >
							<input type="radio" name="state" value="0"/>
							隐藏
						</label>
					</div>
				</div>
			</form>
		</div>
	 	<script>
	        
	        function submitHandler() {
		        if ($.validate.form()) {
		            $.operate.save("/action/category.php?method=add", $('#form-cate-add').serialize());
		        }
		    }
	
	    </script>
	</body>
</html>
