### 项目介绍
php01 DEMO<br>
php02 网站显示<br>
php03 网站后台维护


### 全局参数配置说明
1. phpdemo/php03/view/works/edit.php <br>
测试环境: define("IMGPATH", "D:/code/phpdemo/php02"); <br>
生产环境: define("IMGPATH", "/htdocs"); <br>

2. phpdemo/php03/view/works/add.php <br>
测试环境: $("#forntImage").attr("src", "http://www.xumzwh.com" + res.src); <br>
生产环境: $("#forntImage").attr("src", "http://www.seamore.cn" + res.src); <br>

3. phpdemo/php03/view/works/detail.php <br>
测试环境: var server_url = "http://www.xumzwh.com"; <br>
生产环境: var server_url = "http://www.seamore.cn"; <br>